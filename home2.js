import React from "react";
import {Text, View, StyleSheet, Image, TextInput, ScrollView } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import UploadImage from "./customComponent/displayUploadPic.js";
import {Picker} from 'react-native';
import {connect} from "react-redux";
import {saveProfile} from "./reducer";
import { Button } from 'react-native-elements';



class Home2 extends React.Component {
  state = {
    firstName : this.props.profile.name,
    lastName : this.props.profile.lastName,
    address : this.props.profile.address,
    city : this.props.profile.city,
    pickerColorFlag : "#C7C7CD",
    error: "",
    saveButtonPressed : false
  }

  firstNameChange = (value) => {
    this.removeErrorMsg()
    this.setState({
      firstName : value
    })
  }

  lastNameChange = (value) => {
    this.removeErrorMsg()
    this.setState({
      lastName : value
    })
  }


  addressChange = (value) => {
    this.removeErrorMsg()
    this.setState({
      address : value
    })
  }


  saveProfileOnServer = async() => {
    //first change the animation of button to be loading
    this.setState({saveButtonPressed : true})

    if(!this.props.profile.temp.localUri && !this.props.profile.profilePic)
    {
      //display error asking to submit a photo
      this.setState({
        error : "Please attach a display pic first",
        saveButtonPressed : false})
    }
    else if(!this.props.profile.temp.localUri && this.props.profile.profilePic)
    {
      //updating Profile with an old profile pic
      let response = await fetch(`${this.props.url}saveProfile`, {
        method : "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body : JSON.stringify({
          name : this.state.firstName,
          lastName : this.state.lastName,
          address : this.state.address,
          city : this.state.city,
          api_token : this.props.api,
        })
      })

      let result = await response.json()
      console.log(result)
      if(result.error)
      {
        this.setState({
          error : result.error,
          saveButtonPressed : false})
      }
      else if(result.success)
      {
        this.props.navigation.navigate("mainHome")
      }
    }
    else
    {
      /*The new file will be taken from temp, or if user just updating other
      info then the old display pic thats already in db will be used*/
      let localUri = this.props.profile.temp.localUri || this.props.profile.profilePic
      //extract the filetype
      let fileType = localUri.substring(localUri.lastIndexOf(".") + 1);
      console.log(fileType)
      //let fileType = localUri.substring(localUri.lastIndexOf(":") + 1,localUri.lastIndexOf(";")).split("/").pop();
      console.log("type:", fileType)

      let formData = new FormData();

      formData.append("photo", {
        uri : localUri,
        name: `photo.${fileType}`,
        type: `image/${fileType}`
      });

      //adding profiledata in formData
      let data = JSON.stringify({
        name : this.state.firstName,
        lastName : this.state.lastName,
        address : this.state.address,
        city : this.state.city,
        api_token : this.props.api,
      })

      //let data = {name : this.state.firstName}

      formData.append("data", data )

      console.log("formdata", formData)

      let options = {
        method: "POST",
        body: formData,
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data"
        }
      };

      try
      {
        let response = await fetch(`${this.props.url}saveProfile`, options)
        let result = await response.json()
        console.log(result)
        //if server returns a validation error
        if(result.error)
        {
          this.setState({
            error : result.error,
            saveButtonPressed : false})
        }
        else if(result.success)
        {
          console.log(result)
          /*Bug fix: Need to empty the props.profile.temp.localUri at the
          end of submitting profile data so that that photo doesnt remain in
          apps memory*/
          let newProfileData = {...this.props.profile}
          newProfileData.temp.localUri = ""
          //change this.props.profile.profilePic with new pic link thats gotten from server
          newProfileData.profilePic = result.profileLink
          //navigate to home page and save new profile in store
          this.props.addProfile(newProfileData)
          this.props.navigation.navigate("mainHome")
        }


      }
      catch (e)
      {
        this.setState({
          error : "Something went wrong, please try again later",
          saveButtonPressed : false})
      }


    }

  }

  removeErrorMsg = () => {
    this.setState({error : ""})
  }


  render() {
    return(
      <View style = {{flex : 1, backgroundColor : "#fff"}}>
      <ScrollView >
      <View style = {styles.container}>
        <View style = {{marginTop : "5%"}}>
          <UploadImage/>
        </View>

        <TextInput
          placeholder = "First Name"
          onChangeText = {this.firstNameChange}
          style = {styles.input}
          value = {this.state.firstName}/>

        <TextInput
          placeholder = "Last Name"
          onChangeText = {this.lastNameChange}
          style = {styles.input}
          value = {this.state.lastName}/>

        <TextInput
          placeholder = "Address"
          onChangeText = {this.addressChange}
          style = {styles.input}
          value = {this.state.address}/>

        <View style={styles.picker}>
          <Picker
            selectedValue={this.state.city}
            style={{color : this.state.colorFlag}}
            onValueChange={(itemValue, itemIndex) => {
              this.removeErrorMsg()
              if(itemValue !== "0")
              {
                this.setState({
                  city: itemValue,
                  pickerColorFlag : "black"})
              }
              else
              {
                this.setState({pickerColorFlag : "#C7C7CD"})
              }
            }


            }>

            <Picker.Item label="Select a City" value="0" color = "#C7C7CD" />
            <Picker.Item label="Karachi" value="Karachi" />
            <Picker.Item label="Lahore" value="Lahore" />
            <Picker.Item label="Islamabad" value="Islamabad" />
          </Picker>
        </View>

        <View >
          <Text style = {styles.error}>{this.state.error}</Text>
        </View>


        <View style = {{width : 100 , marginTop: "2%"}}>
          <Button
            title = "Save"
            buttonStyle ={{ backgroundColor: "forestgreen"}}
            onPress = {this.saveProfileOnServer}
            loading = {this.state.saveButtonPressed}/>
        </View>


      </View>
      
    </ScrollView>
  </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    //flex: 1,
    backgroundColor : "#fff",
    alignItems :"center"
  },
  profilePicContainer : {
    borderColor : "aqua",
    borderRadius : 5,
    borderWidth : 3,
    width : 175,
    height : 175,
  },
  changePicIcon : {
    //backgroundColor : "blue",
    height : 50,
    width : 50,
    position : "absolute",
    top : "69%",
    left: "69%",
  },
  input : {
    padding : 10,
    borderBottomWidth:  2,
    borderColor : "grey",
    marginTop : 15,
    width : "60%"
  },
  picker : {
    width: "60%",
    borderBottomWidth:  2,
    borderColor : "grey",
    padding : 10,
  },
  error : {
    padding : 4,
    color : "red"
  },
})


const mapStateToProps = (state) => ({
  api : state.api_token,
  url : state.serverUrl,
  profile : state.profile
})


export default connect(mapStateToProps, {addProfile : saveProfile})(Home2)
