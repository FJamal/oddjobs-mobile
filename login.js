import React from "react";
import {Text, View, StyleSheet, TextInput, TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import {saveApiToken} from "./reducer.js";
import store from "./store.js";
import * as SecureStore from 'expo-secure-store';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Button} from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";


class Login extends React.Component {

  state = {
    email : "",
    password : "",
    loginError : false,
    errorMsg : "",
    LbuttonAnimation : false
  }

  emailChange = (text) => {
    this.setState({
      email : text,
      loginError : false
    })
  }

  passwordChange = (value) => {
    this.setState({
      password : value,
      loginError : false
    })
  }


  handleSubmit = async() => {
    //start the button animation
    this.setState({LbuttonAnimation : true})
    try
    {
      let response = await fetch(`${this.props.url}login`, {
        //mode : "no-cors",
        method : "POST",

        header : {
          "Content-Type" : "application/json",
          "Accept" : "application/json",
          //"Access-Control-Allow-Origin" : "*"
        },
        body : JSON.stringify({
          "email" : this.state.email,
          "password" : this.state.password
        })
      })

      let result = await response.json()
      if (result.error)
      {
        this.setState({
          loginError : true,
          errorMsg : result.error,
          LbuttonAnimation : false
        })
      }
      else
      {
        console.log("store in login before login", store.getState())
        //add api_token in store
        this.props.addToken(result.apiToken)
        console.log("store in login after login", store.getState())
        //storing in memory using secureStore or AsyncStorage
        try
        {
          //save api token in memory for android
          await SecureStore.setItemAsync("apiToken", result.apiToken)
          //navigate to main tab
          this.props.navigation.navigate("Main")
        }
        catch (e)
        {
          if(e.message == "The method or property SecureStore.setItemAsync is not available on web, are you sure you've linked all the native dependencies properly?")
          {
            //use AsyncStorage for web
            await AsyncStorage.setItem("apiToken", result.apiToken)
            //navigate to main tab
            this.props.navigation.navigate("Main")
          }
          else
          {
            throw 500
            this.setState({LbuttonAnimation : false})
          }
        }
      }
    }
    catch (e)
    {
      console.log(e)
      this.setState({
        loginError : true,
        errorMsg : "Something went wrong, please try again later",
        LbuttonAnimation : "false"
      })
    }

  }



  test = ()=> {

    this.props.navigation.navigate("register")
  }

  render(){
    // console.log(this.props)
    return (
      <View style= {styles.container}>

        <View style= {{width: 200}}>
          <TextInput
            onChangeText = {this.emailChange}
            keyboardType = "email-address"
            style = {styles.input}
            placeholder = "Email"
            />

          <TextInput
            style = {styles.input}
            onChangeText = {this.passwordChange}
            secureTextEntry = {true}
            placeholder = "Password"/>
        </View>


        {this.state.loginError ? (<Text style = {styles.error}>{this.state.errorMsg}</Text>) : null}


        <TouchableOpacity
          onPress = {() => this.props.navigation.navigate("register")}
          style = {{marginTop : 10, marginBottom : 10, width: 200 }}>
          <Text style ={styles.register}>Register</Text>
        </TouchableOpacity>



        <Button
          title = "LogIn"
          buttonStyle = {{alignItems : "center", backgroundColor : "grey" ,  padding : 10,}}
          containerStyle = {{width : 200,}}

          onPress = {this.handleSubmit}
          loading = {this.state.LbuttonAnimation}/>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor : "#fff",
    alignItems : "center",
    justifyContent : "center",
  },
  input : {
    padding : 10,
    borderBottomWidth:  2,
    borderColor : "grey",
    marginTop : 15,
  },

  error : {
    padding : 4,
    color : "red"
  },
  register : {
    color: "blue",
    textAlign : "right",
    fontWeight : "bold",
    fontSize : 16,

  },
})


const mapStateToProps = (state) => ({
  url : state.serverUrl,
})


export default connect(mapStateToProps, {addToken : saveApiToken})(Login)
