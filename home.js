import React from "react";
import {
  Modal,
  TouchableHighlight,
  FlatList,
  Dimensions,
  Text,
  ScrollView,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Pressable} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {connect} from "react-redux";
import * as ImagePicker from 'expo-image-picker';
import * as SecureStore from 'expo-secure-store';
import {saveProfile} from "./reducer";
import store from "./store.js";
import JobsList from "./jobslist.js";
import ProfileCompleteMessage from "./customComponent/completeProfileMessage.js";
import {Button} from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import { Feather } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';


const Message = (props) => {
  const navigation = useNavigation()
  return (
    <View>
      <Button
        icon={
          props.notification ? (
            <Feather name="message-circle" size={28} color="red" />)
            : (<Feather name="message-circle" size={24} color="white" />)
        }
        title="Messages"
        iconRight = {true}
        containerStyle = {{}}
        buttonStyle = {{}}
        titleStyle = {{marginRight : "5%"}}
        onPress = {()=> {
          props.removeNewMessageNotification()
        }}/>
    </View>
  )
}



const loadJobDetailOfClickedBid = (props , navigation)=> {
  //const navigation = useNavigation()
  navigation.navigate("searchJob", {
    screen : "searchJobDetail",
    params : { job : {
      id : props.data.jobId,
      title : props.data.jobTitle,
      description : props.data.jobDescription,
      image1 : props.data.jobImage1,
      image2 : props.data.jobImage2,
      type :  props.data.jobType,
    }

    }
     })
}




const YourBidsRow = (props)=> {
  const navigation = useNavigation()
  return (
    <TouchableHighlight
      onPress = {props.data.jobCompleted ?
        (()=>console.log("completed clicked"))
        :
        (()=>loadJobDetailOfClickedBid(props, navigation))
        }
      style = {props.data.jobCompleted ? styles.BidJobCompleted : null}
      underlayColor = "antiquewhite">
        <View style= {(props.data.bidAccepted && !props.data.jobCompleted) ?
                      styles.bidsRowContainerAccepted :
                      styles.bidsRowContainer}>
          <View style = {styles.userInfo}>
            <Image
              source = {{uri : props.data.userProfilePic}}
              style = {{width : "60%", height : "60%", resizeMode : "stretch"}}/>
            <Text style = {{fontWeight : "bold",fontSize : 10}}>{props.data.userName}</Text>
          </View>
          <View style = {styles.titleContainer}>
            <Text style = {{flex : 1,}}>{props.data.jobTitle}</Text>



          </View>
          <View style = {styles.bidStatusContainer}>
            <View style = {{flexDirection : "row",}}>
              <View><Text style = {{fontWeight : "bold", fontSize : 20}}>Bid:</Text></View>
              <View><Text style = {{fontSize : 20}}>Rs {props.data.bidAmount}</Text></View>
            </View>
            <View style = {{flexDirection : "row",}}>
              <View><Text style = {{fontWeight : "bold",}}>Status:</Text></View>
              {
                props.data.jobCompleted ? (
                  <View style = {{marginBottom : "2%"}}>
                    <Text style = {{color : "white"}}>Completed!</Text>
                  </View>
                ) : (
                  <View><Text>{props.data.bidAccepted? "Accepted" : "Pending"}</Text></View>
                )
              }

            </View>
            {
              props.data.bidAccepted ? (
                <Button
                  icon={
                    <Feather name="message-circle" size={24} color="white" />
                  }
                  title="Message"
                  iconRight = {true}
                  containerStyle = {{}}
                  buttonStyle = {{}}
                  titleStyle = {{marginRight : "5%"}}
                  disabled = {props.data.jobCompleted ? true : false}
                  onPress = {()=> {
                    navigation.navigate("messageWindow", {userIdOfChat : props.data.userId})
                  }}/>
              ) : null
            }

          </View>
      </View>
    </TouchableHighlight>
  )
}



const BidList = (props)=> {
  return (
    <FlatList
      data = {props.data}
      renderItem = {renderBidList}
      keyExtractor = {(item, index)=>index.toString()}/>
  )
}


const renderBidList = (obj)=> {
  return (
    <YourBidsRow data = {obj.item}/>
  )
}


class Home extends React.Component {
  state = {
    profile : {
      name : "",
      profilePic : null

    },
    postedJobsActivated : true,
    //testData : testData
    jobs : "",
    bidsData : [],
    bidsLoading : false,

  }

  componentDidMount(){
    console.log(this.props.api, "HOME LOADING")
    //TEST
    this._unsubscribe = this.props.navigation.addListener('focus', async() => {
      console.log("HOME SCREEN RENDERED AGAIN")
      let response  = await fetch(`${this.props.url}settings`, {
        method : "POST",
        header : {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
        },
        body : JSON.stringify({
          apiToken : this.props.api
        })
      })

      let result = await response.json()
      let profile = result.profile
      // reversing the jobs coming from server so that the latest job appears on top
      let jobs = result.jobs.reverse()
      //console.log("result in home is", result)
      this.setState({
        profile, jobs
      })

      /*adding a temperaory object in profile for temp data
      like UploadImage component which will use it
      if user uploads a new image*/
      profile.temp = {}

      //console.log("State IN HOME", this.state)
      //adding to profile to store
      this.props.addProfile(profile)
      console.log("STORE AFTER ADDING PROFILE", store.getState())

    });
    //END TEST



  }

  componentWillUnmount() {
    //removing the eventlistener for focus on the screen
    //this._unsubscribe.remove()
    this._unsubscribe();
  }


  flipActiveJobTab = () => {
    this.setState(previousstate => ({
      postedJobsActivated : !previousstate.postedJobsActivated
    }))
  }

  handleJobsChange = () => {
    //first make the selected tab as active
    this.flipActiveJobTab()
    //TEST
    // this.setState({
    //   testData : testData
    // })
  }

  signout = async() => {
    //remove the apiToken from memory
    await SecureStore.deleteItemAsync("apiToken")
    //remove profile from store
    this.props.addProfile({})
    //sending user back to login page
    this.props.navigation.navigate("loadingPage")
  }

  //When Your Bids tab is clicked
  handleBidsTab = async() => {
    //check if user is already on that tab then do nothing else switch
    if(this.state.postedJobsActivated)
    {
      //change the tab activated flag and start animation for loading bids data
      this.setState({postedJobsActivated : false, bidsLoading : true})
      console.log("already on the tab")
      //get the list of bids user have made of different jobs
      let response = await fetch(`${this.props.url}getuserbids`, {
        method: "POST",
        headers : {
          "Content-Type" : "application/json",
          "Accept" : "application/json",
        },
        body : JSON.stringify({
          api_token : this.props.api
        })
      })

      let result = await response.json()
      console.log(result)
      //load the bids data returned from server and stop the loading animation
      this.setState({bidsData : result.data.reverse(), bidsLoading : false})

    }

  }

  handlePostedJobsTab = () => {
    //check if user is already on that tab then do nothing else switch
    if(!this.state.postedJobsActivated)
    {
      this.setState({postedJobsActivated : true})
    }

  }

  //used by the JobsList imported from jobslist.js to be used in onPress
  handleJobPress = (job) => {
    console.log("Handle Press activated")
    //console.log(job)
    this.props.navigation.navigate("jobDetail", {job})
  }




  newMessageSeen = async ()=> {
    if(this.props.profile.MessageNotification)
    {
      let response  = await fetch(`${this.props.url}removemessagenotification`, {
        method : "POST",
        headers : {
          "Content-Type" : "application/json",
          "Accept" : "application/json",
        },
        body : JSON.stringify({
          user_id : this.props.profile.id
        })
      })
      let result = await response.json()
      console.log(result)
    }
    this.props.navigation.navigate("inbox")

  }



  render(){
    //console.log("state in render", this.state)
    const {modalVisible} = this.state
    if(this.state.profile.profilePic === null && this.props.api && !this.state.profile.name)
    {
      //code to render a loading circle while app loads users info from server
      return(
        <View style = {styles.container}>
          <Image source = {require("./ajax-loader.gif")} style = {{width : 100, height : 100}}/>
        </View>
      )
    }
    else if(this.state.profile.profilePic === null && this.state.profile.name)
    {
      //SECTION THAT ASKS TO COMPLETE THE PROFILE FIRST
      return (
        <ProfileCompleteMessage/>
        )
    }

    else
    {
      //THE MAIN PAGE
      return (
        <View style = {styles.container2}>
          <View style = {{height : "23%", width : "100%", flexDirection : "row", marginTop: "3%", justifyContent : "center",}}>
            <View style = {{width : "30%", height : "100%", alignItems :"center"}}>
              <Image source = {{uri : this.props.profile.profilePic}}
                    style = {{width : "90%", height : "90%", resizeMode : "stretch"}}/>
            </View>
            <View style = {styles.sinOutEditPanel}>
              <Message
                notification ={this.state.profile.MessageNotification}
                removeNewMessageNotification = {this.newMessageSeen}/>
              <Button title = "Edit Profile" onPress = {() => this.props.navigation.navigate("profile")}/>
              <Button title = "Sign Out" onPress = {this.signout}/>
            </View>
          </View>
          <View style = {styles.nameCityContainer}>
            <Text style = {styles.info}>{this.props.profile.name + " " + this.props.profile.lastName} </Text>
            <Text>{this.props.profile.city}, Pakistan</Text>
          </View>


          <View style = {styles.jobsWorkContainer}>
            <TouchableOpacity onPress = {this.handlePostedJobsTab}>
              <Text style = {this.state.postedJobsActivated ? (styles.jobsHeadingActive) : (styles.jobsHeadings)}>Posted Jobs</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress = {this.handleBidsTab}>
              <Text style = {this.state.postedJobsActivated ? (styles.jobsHeadings) : (styles.jobsHeadingActive)}>Your Bids</Text>
            </TouchableOpacity>
          </View>

          {
            (this.state.jobs.length == 0 && this.state.postedJobsActivated) ? (
              <View style = {{marginTop : "3%"}}>
                <Text>You have not posted any jobs as yet</Text>
              </View>
            ) : null
          }
          {
            (this.state.postedJobsActivated) ? (
              <View style = {{flex : 1}}>
                <JobsList
                  jobsData = {this.state.jobs}
                  handleJobPress = {this.handleJobPress}/>
              </View>
            ) : (

              <View style = {{marginTop : "3%", flex : 1}}>
              {
                (this.state.bidsLoading && this.state.bidsData.length == 0) ? (
                  <View style = {{alignItems : "center"}}>
                    <Image source = {require("./ajax-loader.gif")}/>
                  </View>
                ) : null
              }
                {
                  (this.state.bidsData.length == 0 && !this.state.bidsLoading) ? (
                    <Text>You have not posted any Bids as yet</Text>
                  ): (
                    <BidList
                      data = {this.state.bidsData}
                      modalFunction = {this.setModalVisible}/>
                  )
                }
              </View>

          )
          }


        </View>


      )
    }
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor : "#fff",
    alignItems : "center",
    justifyContent : "center",
  },
  profilePicContainer : {
    borderColor : "aqua",
    borderRadius : 5,
    borderWidth : 3,
    width : 100,
    height :100 ,


  },
  container2 : {
    flex: 1,
    backgroundColor : "#fff",
    alignItems : "center",
    //top: "3%",

  },
  // sinOutEditPanel : {
  //   //width : "22%",
  //   //backgroundColor : "green",
  //   height : "30%",
  //   //height : 175,
  //   //left : "66%",
  //   justifyContent : "space-between",
  //   //position : "absolute",
  //   //top: "37%",

  sinOutEditPanel : {
    //width : "22%",
    //backgroundColor : "green",
    //height : "40%",
    //height : 175,
    //left : "66%",
    justifyContent : "space-between",
    //position : "absolute",
    //top: "37%",
    marginLeft : "2%"


  },
  info : {
    fontSize : 20,
    fontWeight : "bold"
  },
  nameCityContainer : {
    alignItems : "center",
    top : "0.5%" ,
    //backgroundColor : "lime",
    width : "70%",
  },
  jobsWorkContainer : {
    //backgroundColor : "yellow",
    flexDirection : "row",
    //top: "3%",
    width : "100%",
    justifyContent : "space-evenly",
    borderBottomColor : "antiquewhite",
    borderBottomWidth : 2,
    //marginBottom : "3%"


  },
  jobsHeadingActive : {
    fontSize : 20,
    fontWeight : "bold",
    color : "blue",
    padding : 20,
    borderBottomColor : "blue",
    borderBottomWidth : 5

  },
  jobsHeadings : {
    fontSize : 20,
    fontWeight : "bold",
    color : "blue",
    padding : 20,
  },

  //Start of styling for bid list

  bidsRowContainer : {
    width : Math.round(Dimensions.get("window").width),
    height : Math.round(Dimensions.get("window").height) * 15/100,
    //backgroundColor : "yellow",
    flexDirection : "row",
    borderBottomColor : "grey",
    borderBottomWidth : 2
  },
  bidsRowContainerAccepted : {
    width : Math.round(Dimensions.get("window").width),
    height : Math.round(Dimensions.get("window").height) * 15/100,
    //backgroundColor : "yellow",
    flexDirection : "row",
    borderBottomColor : "grey",
    borderBottomWidth : 2,
    backgroundColor : "#fff9c4",
  },

  userInfo : {
    width : "18%",
    alignItems : "center",
    //flex : 1
    marginRight : "1%",
  },
  titleContainer : {
    width : "50%",
    alignItems : "center",
    padding : 5,
    justifyContent : "center",
    //backgroundColor : "blue"

  },
  bidStatusContainer : {
    alignItems : "center",
    width : "30%"
  },


  BidJobCompleted : {
    backgroundColor : "#616161"
  }
})


const mapStateToProps = (state) => ({
  api : state.api_token,
  url : state.serverUrl,
  profile : state.profile,
})

export default connect(mapStateToProps, {addProfile : saveProfile})(Home)
