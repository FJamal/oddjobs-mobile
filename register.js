import React from "react";
import {Text, View, StyleSheet, TextInput} from "react-native";
import {connect} from "react-redux";
import {saveApiToken} from "./reducer.js"
import store from "./store.js";
import * as SecureStore from 'expo-secure-store';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Button} from "react-native-elements";


class Register extends React.Component {
  state = {
    name : "",
    lastName : "",
    email : "",
    password : "",
    confirmation : "",
    errorFlag : false,
    errorMsg : "",
    buttonAnimation : false
  }


  nameChange = (text) => {
    //remove the error msg if there was any
    this.removeErrorMsg()

    this.setState({
      name : text
    })
  }


  lastNameChange = (text) => {
    //remove the error msg if there was any
    this.removeErrorMsg()

    this.setState({
      lastName : text
    })
  }

  emailChange = (text) => {
    //remove the error msg if there was any
    this.removeErrorMsg()

    this.setState({
      email : text
    })
  }


  passChange = (text) => {
    //remove the error msg if there was any
    this.removeErrorMsg()

    this.setState({
      password : text
    })
  }


  confirmationChange = (text) => {
    //remove the error msg if there was any
    this.removeErrorMsg()

    this.setState({
      confirmation : text
    })
  }


  handleSubmit = async () => {
    //start the animation of the button
    this.setState({buttonAnimation : true})
    try {
      let response = await fetch(`${this.props.url}register` , {
        method : "POST",
        header : {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
        },
        body : JSON.stringify({
          "name" : this.state.name,
          "last_name" : this.state.lastName,
          email : this.state.email,
          password : this.state.password,
          password_confirmation : this.state.confirmation,

        })
      })

      let result = await response.json()
      if(result.error)
      {
        this.setState({
          errorFlag : true,
          errorMsg : result.error,
          buttonAnimation : false,
        })
      }
      else
      {
        //save apitoken in redux store
        this.props.addToken(result.api_token)
        console.log(store.getState())
        try
        {
          //save api token in memory for android
          await SecureStore.setItemAsync("apiToken", result.api_token)
          //navigate to main tab
          this.props.navigation.navigate("Main")
        }
        catch(e) {
          if(e.message == "The method or property SecureStore.setItemAsync is not available on web, are you sure you've linked all the native dependencies properly?")
          {
            //use AsyncStorage for web
            await AsyncStorage.setItem("apiToken", result.api_token)
            //navigate to main tab
            this.props.navigation.navigate("Main")
          }
          else
          {
            throw 500
            this.setState({buttonAnimation : false})
          }

        }


      }
    }
    catch (e)
    {
      this.setState({
        errorFlag : true,
        errorMsg : "something went wrong, please try later",
        buttonAnimation : false
      })
      console.log(e.message)
    }

  }


  removeErrorMsg = ()=> {
    this.setState({
      errorFlag : false,
    })
  }


  render(){
    return (
      <View style= {styles.container}>
        <View style={{width : 200,}}>
        <TextInput style ={styles.input}
          placeholder = "First Name"
          onChangeText = {this.nameChange}/>

        <TextInput style ={styles.input}
          placeholder = "Last Name"
          onChangeText = {this.lastNameChange}/>

        <TextInput style ={styles.input}
          placeholder = "Email"
          onChangeText = {this.emailChange}
          keyboardType = "email-address"/>

        <TextInput style ={styles.input}
          placeholder = "Password"
          onChangeText = {this.passChange}
          secureTextEntry = {true}/>

        <TextInput style ={styles.input}
          placeholder = "Confirmation"
          onChangeText = {this.confirmationChange}
          secureTextEntry = {true}/>

        {this.state.errorFlag ? (<Text style = {styles.error}>{this.state.errorMsg}</Text>) : null}

        <View style = {{marginTop : 18}}>
          <Button
            title = "Register"
            onPress = {this.handleSubmit}
            buttonStyle = {{alignItems : "center", padding : 10,}}
            loading = {this.state.buttonAnimation}/>
        </View>

        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor : "#fff",
    alignItems : "center",
    justifyContent : "center",
  },
  input : {
    padding : 10,
    borderBottomWidth:  2,
    borderColor : "grey",
    marginTop : 15,
  },
  error : {
    padding : 4,
    color : "red"
  }
})



const mapStateToProps = (state) => ({
  url : state.serverUrl,

})


export default connect(mapStateToProps, {addToken : saveApiToken})(Register)
