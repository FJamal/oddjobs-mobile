import React from "react";
import {View, Text, StyleSheet, Keyboard, TouchableOpacity , Picker, Image} from "react-native";
import ProfileCompleteMessage from "./customComponent/completeProfileMessage.js";
import {connect} from "react-redux";
import { SearchBar } from 'react-native-elements';
import Ionicons from "react-native-vector-icons/Ionicons";
import Constants from 'expo-constants';
import Joblist from "./jobslist.js";


const List = (props) => (
  <Picker
    selectedValue={props.selectedValue}
    style={styles.picker}
    mode = "dropdown"
    dropdownIconColor = "#000000"
    onValueChange= {props.handleChange}
    >
    <Picker.Item label={props.selectionMesg} value="0" color = "#9e9e9e" />
    {
      props.data.map((value, key) => (
        <Picker.Item label  = {value} value = {value} key = {key}/>
      ))
    }
  </Picker>
)





class JobSearchPage extends React.Component {
  state = {
    jobType : "",
    searchCity : "",
    loading : false,
    data : [],
    errorFlag : false,
    errorMsg : "something went wrong pls try again later"
  }

  updateSearch = (search) => {
    this.setState({ search });
  };

  //Handles value change for picker for job type
  handleJobTypeChange = async (itemValue, itemIndex) => {
    console.log("Value selected :", itemValue)
    if(itemValue !== "0")
    {
      await this.setState({jobType : itemValue})
    }
    console.log("state in jobsearch", this.state)
    let result = await this.sendSearchRequest()
    this.setState({data : result.jobs})
  }


  //handles value change for picker for city
  handleCityChange = async (itemValue, itemIndex) => {
    if(itemValue !== "0")
    {
      await this.setState({searchCity : itemValue})
    }
    let result = await this.sendSearchRequest()
    this.setState({data : result.jobs})
  }

//send a post request with city and job type to filter for jobs to search
  sendSearchRequest = async () => {
    //remove any displayed errors first
    this.setState({errorFlag : false})
    try
    {
      //start the loading animation
      this.setState({loading : true})
      console.log("state outside:", this.state)
      let response = await fetch(`${this.props.url}searchjob`, {
        method : "POST",
        headers : {
          "Content-Type" : "application/json",
          Accept : "application/json"
        },
        body : JSON.stringify({
          jobtype : this.state.jobType,
          city : this.state.searchCity,
          userId : this.props.profile.id
        })
      })

      let result = await response.json()
      //stop the loading animation
      this.setState({loading : false})
      console.log(result)
      return result
    }
    catch (e)
    {
      this.setState({errorFlag : true, loading : false})
    }

  }

  //to be used by Joblist imported from joblist.js for selection of a job
  handleSelectedJob = (job) => {
    console.log("job selected", job)
    this.props.navigation.navigate("searchJobDetail", {job})
  }


  render() {
    if(this.props.profile.profilePic === null)
    {
      return (<ProfileCompleteMessage/>)
    }
    else
    {
      //const {search} = this.state

      return (
        <View style = {styles.container}>

          <View style = {styles.searchBoxesContainer}>
            <View style = {styles.jobTypeContainer}>
              <List
                selectedValue = {this.state.jobType}
                selectionMesg = "Select Job Type"
                data = {["plumber", "electrician" , "carpenter" , "painter"]}
                handleChange = {this.handleJobTypeChange}/>
            </View>
            <View style = {styles.searchCityContainer}>
              <List
                selectedValue = {this.state.searchCity}
                selectionMesg = "Select City"
                data = {["Karachi", "Lahore" , "Islamabad" , ]}
                handleChange = {this.handleCityChange}/>
            </View>

          </View>
          <View style = {styles.searchBoxUnderline}></View>
          {
            (this.state.jobType || this.state.searchCity) ? (
              <View style = {styles.availableJobsHeading}>
                <Text style = {{fontWeight : "bold", fontSize : 20}}>Available Jobs</Text>
              </View>
              ) : null
          }

          {
            //logic to display no jobs mesg
            (!this.state.loading && this.state.data.length == 0
              && (this.state.searchCity || this.state.jobType) && !this.state.errorFlag) ? (
              <View style = {{alignItems : "center", padding : "1%"}}>
                <Text>No jobs at the moment</Text>
              </View>
            ) : null
          }

          {
            //Display error if something goes wrong in sending request
            this.state.errorFlag ? (
              <View style = {styles.error}>
                <Text style ={{color : "red"}}>{this.state.errorMsg}</Text>
              </View>
            ) : null
          }

          {this.state.loading ?
            //loading icon
            (
            <View style = {{marginTop : "3%"}}>
              <Image source = {require("./ajax-loader.gif")}/>
            </View>
          ) :
            //Flat list for jobs
            (
              <Joblist
              jobsData = {this.state.data}
              handleJobPress = {this.handleSelectedJob}/>
            )
          }








        </View>
      )
    }

  }
}


const styles = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor : "#ffffff",
    alignItems : "center",
    //justifyContent : "center",
    marginTop : Constants.statusBarHeight
  },
  searchContainer : {
    backgroundColor : "#fafafa",
    width : "80%",
    padding : "2%",
    borderRadius : 16
  },
  searchtab : {
    backgroundColor : "#eceff1",
    width: "96%",
    flexDirection : "row",
    padding : "3%",
    borderRadius : 12,
    alignItems : "center"
  },
  searchBoxesContainer : {
    width : "95%",
    flexDirection : "row",
    top : "0.5%",
    marginBottom : "1%",
  },
  jobTypeContainer : {
    width : "50%",
    alignItems : "center",
    backgroundColor : "#eceff1",
    borderRadius : 18
  },
  searchCityContainer : {
    width : "40%",
    backgroundColor : "#eceff1",
    alignItems : "center",
    justifyContent : "center",
    borderRadius : 18,
    marginLeft : "1%",

  },
  picker : {
    width : "90%",
    //backgroundColor : "#eceff1",
  },
  searchBoxUnderline : {
    borderBottomWidth : 2,
    borderColor : "grey",
    width : "100%",
    marginTop : "1%",
  },
  availableJobsHeading : {
    //marginTop : "5%" ,
    padding : "3%",
    borderBottomWidth : 1,
    borderBottomColor : "black",
    width : "100%",
    alignItems : "center",
    backgroundColor : "#ffe6e6",
  },
  error : {
    alignItems : "center",
    padding : "1%",
    color : "red",
  }
})


const mapStateToProps = (state) => ({
  profile : state.profile,
  url : state.serverUrl,
})


const SearchBox = (props) => (
  <TouchableOpacity style = {styles.searchContainer}>
    <View style = {styles.searchtab}>
    <Ionicons name="ios-search" size={24} color="black" />
    <Text style = {{fontSize : 20, color : "#9e9e9e"}}>{props.placeHolder}</Text>
    </View>
  </TouchableOpacity>
)

// <SearchBar
//   placeholder="Type Here..."
//   onChangeText={this.updateSearch}
//   value={search}
//   containerStyle = {{width : "90%", backgroundColor : "#fafafa", borderColor : "yellow"}}
//   inputContainerStyle = {{backgroundColor : "#eeeeee"}}
//   onFocus = {() => {
//     //console.log("search bar in focus")
//     Keyboard.dismiss()
//   }}
//   />





export default connect(mapStateToProps)(JobSearchPage)
