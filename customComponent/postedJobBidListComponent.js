import React from "react";
import {Image, Dimensions, View,FlatList, Text, ScrollView, StyleSheet, TouchableHighlight} from "react-native";
import { Button, Rating, AirbnbRating} from 'react-native-elements';
import { Feather} from '@expo/vector-icons';

const HEIGHT = Dimensions.get("window").height


const BidsList = (props)=> {
  return(
    <FlatList
      renderItem = {(item)=>renderBids(item, props)}
      data = {props.Data}
      keyExtractor = {(item,index)=> index.toString()}
      />
  )

}


const renderBids = (obj, props)=> {

  return (
    <View
      key= {obj.item.key}
      style = {obj.item.accepted ? (styles.bidsListItemSelected) :(styles.bidsListItem)}>
      <View style = {styles.bidRowInfo}>
        <Text style = {{fontWeight : "bold"}}>
          {obj.item.name + " " + obj.item.lastName}
        </Text>
        <Image
          source = {{uri : obj.item.profilePic}}
          style = {{width : "100%", height : "80%", resizeMode : "stretch"}}/>
        <Rating
          imageSize={26}
          readonly
          startingValue={obj.item.rating}
          style = {{
            marginTop : "5%",

          }}

          tintColor = {obj.item.accepted ? "#fff9c4" : null}
          />
      </View>
      <View style = {styles.bidRowAmount}>
        <Text style = {styles.bidAmount}>Rs.{obj.item.amount}</Text>
          <ScrollView nestedScrollEnabled = {true}>
            <Text style= {{padding : 5}}>{obj.item.comment ? obj.item.comment : "No comments were submitted"}</Text>
          </ScrollView>
      </View>
      <View style = {styles.bidRowButtons}>
        {
          obj.item.accepted ?
           (
            <View>
              <Button
                buttonStyle = {{width : "75%", backgroundColor : "#ef5350"}}
                title = "Cancel"
                onPress = {()=>{props.bidAccepted(obj.index, false, false)}}
                disabled = {props.buttonsDisabled ? true : false}/>


              <Button
                icon={
                  <Feather name="message-circle" size={24} color="white" />
                }
                title="Message"
                iconRight = {true}
                containerStyle = {{width : "75%", marginTop: "5%"}}
                buttonStyle = {{}}
                titleStyle = {{marginRight : "5%"}}
                onPress = {()=>{props.loadChat(obj.item)}}
                disabled = {props.buttonsDisabled ? true : false}/>
            </View>

          ) :
          (
            <Button
              buttonStyle = {{width : "75%"}}
              title = "Accept"
              onPress = {()=> {props.bidAccepted(obj.index, true, true)}}
              disabled = {props.buttonsDisabled ? true : false}/>

          )
        }


      </View>

    </View>
  )
}



const styles = StyleSheet.create({
  bidsListItemSelected : {
    backgroundColor : "#fff9c4",
    width : "100%",
    height :  HEIGHT * 18/100,
    borderBottomWidth : 1,
    borderBottomColor : "grey",
    flexDirection : "row",
    justifyContent : "center",
  },
  bidsListItem : {
    //backgroundColor : "yellow",
    width : "100%",
    height :  HEIGHT * 18/100,
    borderBottomWidth : 1,
    borderBottomColor : "grey",
    flexDirection : "row",
    justifyContent : "center",
  },
  bidRowInfo : {
    width : "30%",
    height : "80%",
    //backgroundColor : "yellow",
    padding : "2%",
    alignItems : "center",
  },
  bidRowAmount : {
    marginLeft : "2%",
    marginRight : "3%",
    justifyContent : "center",
    //backgroundColor : "green",
    width : "40%",
    alignItems : "center"
  },
  bidAmount : {
    fontSize : 22,
    padding : "1%",
    fontWeight : "bold"
  },
  bidRowButtons : {
    justifyContent : "center",
    width : "25%",
    //backgroundColor : "grey"
  },
})


export default BidsList
