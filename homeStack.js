import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import Home from "./home.js";
import Profile from "./home2.js";
import JobDetail from "./postedjobdetail.js";
import Inbox from "./inbox.js";
import MessageWindow from "./messageWindow.js";

const homeStack = createStackNavigator()

function stack (){
  return (
    <homeStack.Navigator>
      <homeStack.Screen name = "mainHome" component = {Home}
      options = {{
        headerShown : false
      }}/>
      <homeStack.Screen name = "profile" component = {Profile}/>
      <homeStack.Screen
        name = "jobDetail"
        component = {JobDetail}
        options = {{
          title : "Posted Job Detail"
        }}/>

      <homeStack.Screen
        name = "inbox"
        component = {Inbox}
        options = {{
          title : "Inbox"
        }}/>

      <homeStack.Screen
        name = "messageWindow"
        component = {MessageWindow}
        options = {{
          title : "Chat",
        }}/>
    </homeStack.Navigator>
  )
}

export default stack;
