import React from "react";
import {TouchableOpacity, Text, View, Image, StyleSheet, FlatList} from "react-native"
import PropTypes from "prop-types";
import {Dimensions} from "react-native";
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';

const renderJobList = (obj, props) =>{
   let source
  // let navigation = obj.item.navigation
  // /*than delete navigation object so that obj.item can be passed without any
  // warning to the jobdetail page */
  // delete obj.item.navigation
  //to solve the bug of sending dynamic require links for image tag in react native
  switch(obj.item.type)
  {
    case "carpenter":
      source= require(`./assets/carpenter.png`)
      break;
    case "painter":
      source = require(`./assets/painter.png`)
      break;
    case "electrician":
      source = require(`./assets/electrician.png`)
      break;
    case "plumber":
      source = require(`./assets/plumber.png`)
      break;
    default :
      source = require(`./assets/briefcase2.png`)
  }
  return (

  <TouchableOpacity
    style = {obj.item.completed ? styles.acceptedJob : ""}
    onPress = {() => props.handleJobPress(obj.item)}>
    <View style={styles.container}>
      <View>
        <Image
          style = {{height : 50, width : 50}}
          source = {source}/>
      </View>
      <View style = {{marginLeft : "0%", width : "70%"}}>
        <Text style = {styles.jobHeading}>{obj.item.title}</Text>
      </View>
      {
        obj.item.completed ? (
          <View style = {{marginLeft : "1%", justifyContent : "center"}}>
            <MaterialIcons name="done" size={40} color="white" />
            <Text style = {{color : "white", fontWeight : "bold"}}>completed</Text>
          </View>
        ) : null
      }


    </View>
  </TouchableOpacity>


)
}


const JobsList = (props) => {
  //to have navigation object available to this component that is not a navigation screen
  //const navigation = useNavigation()

  /*adding the navigation object to every object of the data so that it
  is availabel to every rendered item in renderItem of flatlist*/
  // let data = props.jobsData.map((ob)=> {
  //   return {
  //     ...ob, navigation
  //   }
  // })
  return (
  <FlatList
    renderItem = {(item) =>renderJobList(item, props)}
    data = {props.jobsData}
    keyExtractor = {(item, index) => index.toString()}
    contentContainerStyle = {{bottom : "5%",}}
    />
)
}




JobsList.propTypes = {
  jobsData : PropTypes.array
}

const styles = StyleSheet.create({
  container : {
    padding : 20,
    borderColor: "grey",
    borderBottomWidth : 3,
    alignItems : "center",
    width : Math.round(Dimensions.get("window").width),
    flexDirection : "row",
    marginTop : "3%"

  },
  jobHeading : {
    textDecorationLine : "underline",
    padding: 10,
    fontSize : 20,
    flexShrink : 1
  },
  acceptedJob : {
    //backgroundColor : "#9e9e9e",
    backgroundColor : "#616161"
  }
})



export default JobsList


//const test = ()=> navigation.navigate("jobDetail", obj.item)
