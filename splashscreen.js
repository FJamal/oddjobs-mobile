import React from "react";
import {View, Image, StyleSheet} from "react-native";
import * as SecureStore from "expo-secure-store";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {connect} from "react-redux";
import {saveApiToken} from "./reducer";


class splashScreen extends React.Component {
  componentDidMount(){
    //using eventlistener according to docs to have the screen reload everytime it is visited
    console.log("Inside splashscreen")
    this._unsbscribe = this.props.navigation.addListener("focus", async() => {
      //console.log("Store in core at loading", store.getState())
      let test = await AsyncStorage.getItem("apiToken")
      let token;
      console.log(test)
      try
      {
        //first try to retrieve api_token from secureStore
        token = await SecureStore.getItemAsync("apiToken")
        //console.log("token in splashscreen", token)

        if(token !== null)
        {
          //adding the token into store
          this.props.addToken(token)

          //get users info and login automatically
          this.props.navigation.navigate("Main")
        }
        else
        {
          //take to loginScreen
          this.props.navigation.navigate("login")
        }
      }
      catch (e)
      {
        token = await AsyncStorage.getItem("apiToken")
        console.log("token", token)
        if (token !== null)
        {
          //adding the token into store
          this.props.addToken(token)

          //get users info and login automatically
          this.props.navigation.navigate("Main")

        }
        else
        {
          //take to loginScreen
          this.props.navigation.navigate("login")
        }
      }
      //console.log("Store in core after componentDidMount", store.getState())
    })

  }


  render() {
    return (
      <View style ={styles.container}>
        <Image
          style = {styles.load}
          source = {require("./ajax-loader.gif")}/>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container : {
    flex: 1,
    alignItems : "center",
    backgroundColor : "#fff",
    justifyContent : "center"

  },
  load : {
    width: 100,
    height : 100
  }
})



export default connect(null, {addToken : saveApiToken})(splashScreen)
