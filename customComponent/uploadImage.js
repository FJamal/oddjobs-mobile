/*This component will work with redux where it saves image URI to redux Store
*/

import React from "react";
import {View, Image, StyleSheet, TouchableOpacity, Text} from "react-native";
import * as ImagePicker from 'expo-image-picker';
import {connect} from "react-redux";
import store from "../store";
import {savePostJobImage} from "../reducer";




class UploadImage extends React.Component {
  state = {
    image : ""
  }


  componentDidMount() {
    this.unsubscribe  = this.props.navigation.addListener("focus", () => {
      console.log("Imageupload component loaded")
      this.setState({image : ""})
    })
  }

  componentWillUnmount() {
    this.unsubscribe()
  }


  selectPic = async() => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      //allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    //console.log(result);
    if(!result.cancelled)
    {
      this.setState({
        image : result.uri
      })

      /**THE PLACE TO EDIT FOR DIFFRENT USES**/
      //adding image to redux store
      this.props.addImage(this.state.image)
    }
  }

  removeImage = ()=> {
    this.setState({
      image: ""
    })
  }

  render(){
    return (
      <View style ={styles.profilePicContainer}>
        <View style = {{flex: 1,}}>
          <Image
            source = {
              this.state.image ? ({uri : this.state.image}) : (require("../assets/imageUpload-icon.png"))
            }
            style = {styles.maxHeightWidth}/>
        </View>
        <TouchableOpacity style = {styles.changePicIcon} onPress = {this.selectPic}>
          <Image source = {require("../assets/camera-icon2.png")}
            style = {styles.maxHeightWidth}/>
        </TouchableOpacity>
        <TouchableOpacity
          onPress = {this.removeImage}
          style = {styles.removeImage}>
          <Text style = {styles.X}>X</Text>
        </TouchableOpacity>

      </View>
    )
  }
}


const styles = StyleSheet.create({
  profilePicContainer : {
    width : "100%",
    height : "100%",
    borderColor : "aqua",
    borderRadius : 5,
    borderWidth : 3,
    // width : 175,
    // height : 175,

  },
  changePicIcon : {
    //backgroundColor : "blue",
    height : 50,
    width : 50,
    position : "absolute",
    top : "69%",
    left: "69%",

  },
  maxHeightWidth : {
    width : "100%",
    height : "100%",
    resizeMode: 'stretch'
  },
  removeImage : {
    position : "absolute",
    //backgroundColor : "grey",
    alignSelf : "flex-end",
  },
  X : {
    fontWeight : "bold",
    fontSize : 24,
    color : "tan",
    padding : "5%"
  },
})


const mapStateToProps = (state) => ({
  profile : state.profile
})

export default connect(mapStateToProps, {addImage : savePostJobImage})(UploadImage)
