import React from "react";
import {Text, View, StyleSheet} from "react-native";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import LoginScreen from "./login.js";
import RegisterScreen from "./register.js";
//import Home from "./home.js";
import PostAJobPage from "./secondTab.js";
import Ionicons from "react-native-vector-icons/Ionicons"
import store from "./store";
import SplashScreen from "./splashscreen";
import Home from "./homeStack.js";
import { Entypo } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import SearchAJobStack from "./jobsearchstack.js";


const Tab = createBottomTabNavigator()

const Tabs = () => {
  return(
    //<NavigationContainer>
      <Tab.Navigator initialRouteName = "home"
        screenOptions = {({route}) => ({
          tabBarIcon : ({focused,color, size}) => {
            let iconName;
            if(route.name == "home") {
              iconName = "ios-home"
              return <Ionicons name = {iconName} color = {color} size = {size}/>
            }
            else if(route.name == "postJob")
            {
              //iconName = "md-settings"
              iconName = "briefcase"
              return <Entypo name = {iconName} color = {color} size = {size}/>
            }
            else if (route.name == "searchJob")
            {
              iconName = "briefcase-search"
              return <MaterialCommunityIcons name = {iconName} size={size} color= {color} />
            }

            //return <Ionicons name = {iconName} color = {color} size = {size}/>
          }
        })}
        tabBarOptions = {{
          style : {
            height : 50
          },
          showLabel : false,
          activeTintColor : "gold"
        }

        }>
        <Tab.Screen name="home" component ={Home}/>
        <Tab.Screen name="postJob" component ={PostAJobPage}/>
        <Tab.Screen name="searchJob" component ={SearchAJobStack}/>
      </Tab.Navigator>
    //</NavigationContainer>
  )
}


const Stack = createStackNavigator()

function Stacks() {
  return(
    <NavigationContainer>
      <Stack.Navigator initialRouteName = "loadingPage">
        <Stack.Screen
          name = "loadingPage"
          component = {SplashScreen}
          options = {{
            headerShown : false
          }}/>

        <Stack.Screen
          name ="Main"
          component = {Tabs}
          options = {{
            headerShown : false,
          }}/>
        <Stack.Screen name= "login"
          component = {LoginScreen}
          options = {{
            title : "Login",
            headerTitleAlign : "left"
          }}/>

        <Stack.Screen name= "register"
          component = {RegisterScreen}
          options = {{
            title : "Register",
            headerTitleAlign : "left",
          }}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default class core extends React.Component {

  render(){
    return (
      <Stacks/>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor : "#fff",
    alignItems : "center",
    justifyContent : "center",
  }
})
