import React from "react";
import {
  TextInput,
  View,
  Text ,
  StyleSheet,
  Image,
  ScrollView,
  Modal,
  TouchableHighlight,
  FlatList,
  SafeAreaView,
  Dimensions,
} from "react-native";
import {connect} from "react-redux";
import { Button, Rating, AirbnbRating} from 'react-native-elements';
import { useRoute } from '@react-navigation/native';
import JobDetail from "./customComponent/PostedJobDetailComponent.js";
import BidsList from "./customComponent/postedJobBidListComponent.js";



const HEIGHT = Dimensions.get('window').height;


class PostedJobDetail extends React.Component {

  state = {
    loading : true,
    bids : [],
    acceptedBidIndex : "",
    acceptedBidFlag : false,
    jobCompletedFlag : this.props.route.params.job.completed,
    modalVisible: false,
    bidderName : "",
    bidderProfilePic : "",
    bidderUserId : "",
    bidderCity : "",
    jobCompleteButtonAnimation : false,
    ratingsError : false,
    thanksRateMsg : false,

  };

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener("focus", async() => {
      console.log("Search job detail page loaded")
      let response = await fetch(`${this.props.url}getallbids`, {
        method:"POST",
        headers : {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
        },
        body : JSON.stringify({
          "job_id" : this.props.route.params.job.id
        })
      })

      let result = await response.json()
      console.log(result)
      let {bids} = result
      /*checking if there is accepted bid for the job to let state know
      so the screen can display a job completed button*/
      let flagForAcceptedBid = false
      for(let i = 0; i < bids.length; i++)
      {
        if(bids[i].accepted == true)
        {
          flagForAcceptedBid = true
          this.setState({acceptedBidFlag : true})
          break;
        }
      }
      this.setState({bids, loading : false})
      console.log(this.state)

    })
  }

  componentWillUnmount() {
    this._unsubscribe()
  }

  /*to be used by function in renderItem for <BidsList/> that handles
  bids acception action*/
  bidAccepted = async(value, bool, acceptedBidFlag)=> {
    console.log("Bid accepted with a key:", value)
    //save the key of accepted bid in state
    let bids = this.state.bids
    //to save the id of the bid thats accepted so that id can be used by server
    let bidId;
    let job_id;
    bids = bids.map((item, key)=> {
      //change the rest to unaccepted so that only one bid for a job remains selected
      item.accepted = false
      if(key ==value)
      {
        item.accepted = bool
        bidId = item.id
        /*Need the jobs id for which the bid is accepted or cancellled
        so that it can be used on server to update the bids status*/
        job_id = item.job_id
      }

      return {...item}
    })
    //update bids and set flag to display completed job button
    /*if bool is true, accepted button was clicked so update state
    that there is accepted bid so display a compledted job button*/
    this.setState({bids, acceptedBidFlag})


    //TODO SAVE THIS ON SERVER ALSO MISSING AWAIT KEYWORD
    let result = await this.saveBidStatusOnServer(bool, bidId, job_id)
    console.log("result from server",result)
  }




  saveBidStatusOnServer = async(bool, id, job_id)=> {
    try
    {
      let response = await fetch(`${this.props.url}acceptbid`, {
        method : "POST",
        headers : {
          "Content-Type" : "application/json",
          "Accept" : "application/json",
        },
        body : JSON.stringify({
          bidId : id,
          accepted : bool,
          job_id : job_id,
        })
      })

      let result = await response.json()
      return result
    }
    catch (e)
    {

    }
  }


  //function to load the chat window if user decides to chat with a accepted Bid
  loadChat = (data)=> {
    //console.log("Chat window pressed with data", data);
    this.props.navigation.navigate("messageWindow", {userIdOfChat : data.user_id})
  }


  /*to be used by JobDetail component
  params : job id and boolean to either mark job as completed or uncompleted
  returns : nothing
  effect : loads the modal to vote for the bidder with his info*/
  handleJobCompletion = async(jobId, value) => {
    if(value)
    {
      console.log("INSide")
      //start loading animation if job completed was press
      this.setState({jobCompleteButtonAnimation : true})
    }
    let response = await fetch(`${this.props.url}jobcompleted`, {
      method : "POST",
      headers : {
        "Content-Type" : "application/json",
        Accept : "application/json",
      },
      body : JSON.stringify({
        jobId : jobId,
        value : value,
      })
    })

    let result = await response.json()
    console.log(result)
    this.setState({jobCompletedFlag : value})
    if(result.jobCompleted)
    {
      this.setState({
        bidderName : result.userName,
        bidderProfilePic : result.profilePic,
        bidderUserId : result.user_id,
        bidderCity : result.city,
        jobCompleteButtonAnimation : false,
      })
      this.setModalVisible(true)
    }

  }

  //modal Function
  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  }


  ratingCompleted = async(rating) => {
    try
    {
      //show thank you message
      this.setState({thanksRateMsg : true})
      let response = await fetch(`${this.props.url}saveratings`, {
        method : "POST",
        headers : {
          "Content-Type" : "application/json",
          "Accept" : "application/json",
        },
        body : JSON.stringify({
          userId : this.state.bidderUserId,
          score : rating,
        })
      })
      let result = await response.json()
      console.log(result)
      if(result.ratingsSaved)
      {
        this.setState({thanksRateMsg : false})
        //close the modal
        this.setModalVisible(false)
      }
      else
      {
        this.setState({ratingsError : true})
        setTimeout(()=>{
          this.setState({ratingsError : false})
        }, 3500)
      }
    }catch (e)
    {
      console.log(e)
      this.setState({ratingsError : true})
      //console.log(this.state)
      setTimeout(()=>{
        this.setState({ratingsError : false})
      }, 2000)
    }
  }


  render (){
    //console.log(this.props)
    const description = this.props.route.params.job.description
    //pushing the images in to ann array if there are any so that render can map a compoent for them
    const images = []
    images.push(this.props.route.params.job.image1, this.props.route.params.job.image2)

    const jobId = this.props.route.params.job.id

    const { modalVisible } = this.state
    return(

        <View style = {styles.container}>
          <View style = {{height : HEIGHT * 40/100}}>
            <JobDetail
              id = {jobId}
              handleJobCompletion = {this.handleJobCompletion}
              acceptedBidFlag = {this.state.acceptedBidFlag}
              jobCompletedFlag = {this.state.jobCompletedFlag}
              buttonLoading = {this.state.jobCompleteButtonAnimation}/>
          </View>

          <View style = {styles.bidsHeading}>
            <Text>Bids</Text>
          </View>
          {
            //to display loading animation
            this.state.loading ? (
              <View>
                <Image source = {require("./ajax-loader.gif")}/>
              </View>
            ) : null
          }

          {
            //if no bids were returned from the server
            (!this.state.loading && this.state.bids.length == 0) ? (
              <View>
                <Text style = {{padding : 5}}>No Bids for this job yet</Text>
              </View>
            ) : null
          }
          {
            //bids are returned
            (!this.state.loading && this.state.bids.length > 0) ? (
              <BidsList
                Data = {this.state.bids}
                bidAccepted = {this.bidAccepted}
                loadChat = {this.loadChat}
                buttonsDisabled = {this.state.jobCompletedFlag}/>
            ): null
          }


          {//start of the modal View
          }

          <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!modalVisible);
          }}
        >

            <View style={styles.modalView}>
              <Text style={styles.modalText}>How was your experience with {this.state.bidderName}? </Text>

              <Image
                style = {{width : "40%", height : "40%", resizeMode : "stretch", margin : 20}}
                source = {{uri : this.state.bidderProfilePic}}/>
              <Text style = {{fontSize : 22, fontWeight : "bold"}}>Rate {this.state.bidderName}</Text>




              <AirbnbRating
                size = {60}
                defaultRating = {1}
                onFinishRating = {this.ratingCompleted}/>
              {
                this.state.ratingsError ? (
                  <View>
                    <Text style = {{color : "red"}}>Something Went Wrong Please Try Again Later</Text>
                  </View>
                ) : null
              }

              {
                this.state.thanksRateMsg ? (
                  <View style = {{marginTop : "2%"}}>
                    <Text style = {{color : "green"}}>Thank You For Input</Text>
                  </View>
                ) : null

              }


            </View>

        </Modal>


        </View>
    )
  }
}






const styles = StyleSheet.create({
  container : {
    backgroundColor : "#fff",
    flex : 1,
    //justifyContent : "center",
    alignItems : "center",
    //marginBottom : "18%",

  },
  bidsHeading : {
    padding : "3%",
    borderBottomWidth : 2,
    borderBottomColor : "black",
    borderTopWidth : 2,
    borderTopColor : "black",
    width : "100%",
    alignItems : "center",
    backgroundColor : "#ffe6e6",
    marginTop : "1%",
  //  marginBottom : "15%"
  },

  /*START OF STYLING MODAL FOR VOTING OF BIDDER*/
  centeredView: {
    //flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    //width : "80%",
    //height : "40%",
    margin: 20,
    marginTop : "20%",
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontSize : 20
  }
})


const mapStateToProps = (state) =>({
  url : state.serverUrl,
  api_token : state.api_token,
})


export default connect(mapStateToProps)(PostedJobDetail)
