import React from "react";
import {ScrollView, Text, View, StyleSheet, TextInput, TouchableOpacity} from "react-native";
import ProfileCompleteMessage from "./customComponent/completeProfileMessage.js";
import {connect} from "react-redux";
import SelectList from "./customComponent/selectList.js";
import UploadImage from "./customComponent/uploadImage.js";
import {Button} from "react-native-elements";
//import { Icon } from 'react-native-elements';
//import Icon from 'react-native-vector-icons/MaterialCommunity';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import store from "./store.js";
import {deletePostJobImage} from "./reducer.js";
import { useIsFocused } from '@react-navigation/native';


class Second extends React.Component {
  state = {
    title : "",
    description : "",
    flagforImages : false,
    buttonFlag : false,
    errorFlag : false,
    errorMsg : "",

  }

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener("focus", () => {
      //clear any errors if any errors were displayed in the last tab visit
      //console.log("screen loaded")
      this.setState({errorFlag : false})
    })
  }

  componentWillUnmount() {
    this._unsubscribe()
  }


  jobTitleChange = (value) => {
    this.setState({
      title : value
    })
  }

  changeDescription = (value) => {
    this.setState({
      description : value
    })
  }

  ShowHideImagePost = () => {
    this.setState((prevState) => ({
      flagforImages : !prevState.flagforImages
    }
    ))

    //remove errors if there were any
    if(this.state.errorFlag)
    {
      this.setState({errorFlag : false})
    }
  }

  //Handles Submission
  postJob = async() => {
    //start the loading animation on the button
    this.setState({buttonFlag : true})
    //console.log(this.props.componentData.images)


    //Validation if user clicked for adding images but didnt selected any images
    if(this.state.flagforImages && this.props.componentData.images.length == 0)
    {
      this.setState({
        errorFlag : true,
        errorMsg : "No photos were attached",
        buttonFlag : false,

      })
    }

    if(!this.state.flagforImages)
    {
      //submit job without photos
      let result = await this.submitWithoutImages()
      if(result.error)
      {
        this.displayServerError(result.errormsg)
      }
      else if(result.success)
      {
        //Job been posted, show appropriate action
        //delete the store data associated with selectlist and images
        this.props.deleteStoreData()
        //clear inputFields
        this.clearInputFields()
        //going to homepage
        this.props.navigation.navigate("mainHome");
      }
    }
    else if(this.state.flagforImages && this.props.componentData.images.length !== 0)
    {
      let result = await this.submitWithImages()
      //console.log("TEST", result)
      if(result.error)
      {
        this.displayServerError(result.errormsg)
      }
      else if(result.success)
      {
        /*JOB POSTED SUCCESSFULLY WITH IMAGES*/
        //delete the store data associated with selectlist and images
        console.log("STORE AFTER JOB POST BEFORE STORE DELETION", store.getState())
        this.props.deleteStoreData()
        console.log("STORE AFTER JOB POST AFTER STORE DELETION", store.getState())
        //clear inputFields
        this.clearInputFields()
        //going to homepage
        this.props.navigation.navigate("mainHome");

      }
    }
  }

  submitWithoutImages = async()=> {
    console.log("submitted without photos")
    try
    {
      let response = await fetch(`${this.props.serverUrl}postjob`, {
        method : "POST",
        header: {
          "Content-Type" : "application/json",
          "Accept" : "application/json",
        },
        body: JSON.stringify({
          title : this.state.title,
          description : this.state.description,
          type: this.props.componentData.type,
          api_token : this.props.api_token,
        })
      })

      let result = await response.json()
      //console.log("RESULT", result)
      return result
    } catch(e)
    {
      console.log(e)
      this.displayServerError()
    }
  }

  submitWithImages = async() => {
    console.log("submitted with images")
    //converting photos into formdata to be send via post to server
    let formData = new FormData()
    for( let i = 0; i < this.props.componentData.images.length; i++)
    {
      let localUri = this.props.componentData.images[i]
      let imageName = "photo" + i
      let fileType = localUri.substring(localUri.lastIndexOf(".") + 1)

      formData.append(imageName, {
        uri: localUri,
        name: `${imageName}.${fileType}`,
        type : `image/${fileType}`
      })
    }
    //the data to for job post
    let data = JSON.stringify({
      title : this.state.title,
      description : this.state.description,
      type: this.props.componentData.type,
      api_token : this.props.api_token,
    })

    //adding this to the formdata aswell
    formData.append("data", data)
    //console.log("FORM DATA", formData)
    let options = {
      method : "POST",
      headers : {
        "Content-Type" : "multipart/form-data",
        Accept : "application/json"
      },
      body: formData,
    }

    try
    {
      let response = await fetch(`${this.props.serverUrl}postjob`, options)
      let result = await response.json()
      console.log(result)
      //TODO: NEED TO REMOVE IMAGES FROM STORE AT THE END
      return result
    }
    catch (e)
    {
      console.log(e)
      this.displayServerError()
    }

  }


  displayServerError = (msg = "") => {
    this.setState({
      errorFlag : true,
      errorMsg : msg ? msg : "Something Went Wrong, please try again later",
      buttonFlag : false,
    })
  }

  clearInputFields = () => {
    this.setState({buttonFlag : false, title : "", description : ""})
  }


  render(){
    // Get it from props
    //const { isFocused } = this.props;
//console.log("isFocused:" , isFocused)

    if(this.props.profile.profilePic === null)
    {
      return (<ProfileCompleteMessage/>)
    }
    else
    {
      return (

        <View style= {styles.container}>

          <TextInput
            style = {styles.title}
            placeholder = "Job's Title"
            onChangeText = {this.jobTitleChange}
            value = {this.state.title}/>

          <TextInput
            style = {styles.descriptionBox}
            multiline = {true}
            placeholder ="Description"
            onChangeText = {this.changeDescription}
            numberOfLines = {6}
            value = {this.state.description}/>

          <SelectList
            navigation={this.props.navigation}
            selectionMesg = "Select Job Type"
            data = {["electrician", "plumber", "carpenter", "painter"]}/>

          <View style = {{borderColor : "grey", borderWidth : 1, width : "60%"}}></View>

          {
            this.state.flagforImages ? (
              <View style = {{justifyContent : "center", alignItems : "center", top : "1%"}}>
                <TouchableOpacity onPress = {this.ShowHideImagePost}>
                  <Text style = {styles.addImage}>Remove Images  -</Text>
                </TouchableOpacity>
                <View style = {styles.jobPostImageContainer}>
                  <View style = {{height : "100%", width : "15%", }}>
                    <UploadImage navigation={this.props.navigation}/>
                  </View>
                  <View style = {{height : "100%", width : "15%", marginLeft : "10%" }}>
                    <UploadImage navigation={this.props.navigation}/>
                  </View>
                </View>
              </View>

            )
            :
            (
              <TouchableOpacity style = {{top: "1%"}} onPress = {this.ShowHideImagePost}>
                <Text style = {styles.addImage}>Add Images  +</Text>
              </TouchableOpacity>
            )
          }

          {
            this.state.errorFlag ? (<Text style ={styles.error}>{this.state.errorMsg}</Text>) : null
          }
          <View style = {{top : "3%", width: "100%", alignItems : "center"}}>
            <Button
              buttonStyle = {{padding : 10, }}
              icon = {
                <MaterialCommunityIcons
                  name = "briefcase-upload"
                  size = {30}
                  color = "white"/>

              }
              title = "Post Job"
              titleStyle = {{marginRight : "10%"}}
              iconRight
              loading = {this.state.buttonFlag}
              containerStyle = {{
                width : "50%"
              }}
              onPress = {this.postJob}
              />



          </View>



        </View>
      )
    }

  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor : "#fff",
    alignItems : "center",
    justifyContent : "center",
  },
  descriptionBox : {
    width : "60%",
    textAlignVertical : "top",
    borderColor : "grey",
    borderWidth : 1,
    padding : 3,

  },
  title : {
    width : "60%",
    borderColor : "grey",
    borderWidth : 1,
    padding : 10,
    marginBottom : "3%"
  },
  jobPostImageContainer : {
    flexDirection : "row",
    //backgroundColor : "red",
    width : "100%",
    //justifyContent : "center",
    //alignItems : "flex-start",
    height : 150,
    marginTop : "4%",
    marginBottom : "2%",

  },
  addImage : {
    fontSize : 22,
    textDecorationLine : "underline",
    color : "blue"
  },

  error : {
    padding : 4,
    color : "red",
    fontSize : 20,
  },

})

const mapStateToProps = (state) => ({
  profile : state.profile,
  componentData : state.postJobData,
  serverUrl: state.serverUrl,
  api_token: state.api_token,
})


export default connect(mapStateToProps, {deleteStoreData : deletePostJobImage})(Second)


// export default connect(mapStateToProps, {deleteStoreData : deletePostJobImage})((props) => {
//   const isFocused = useIsFocused();
//   console.log("IN CONNECT:" , isFocused)
//   return <Second {...props} isFocused={isFocused} />;
//
// })
