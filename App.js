import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Core from "./core.js";
import {Provider} from "react-redux";
import store from "./store.js"


export default class App extends React.Component {
  render(){
    return (
      <Provider store = {store}>
        <Core/>
      </Provider>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
