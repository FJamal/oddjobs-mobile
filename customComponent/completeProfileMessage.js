import React from "react";
import {View, StyleSheet, Text, TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import { useNavigation } from '@react-navigation/native';

//const navigation = useNavigation()
function ProfileCompleteMessage (props) {


    const navigation = useNavigation()
    return (
      <View style= {styles.container}>
        <Text>Welcome {props.profile.name}. First complete your profile to view full app</Text>
        <TouchableOpacity
          onPress = {()=> navigation.navigate("profile")}
          >
          <Text style = {{color : "blue"}}>Complete Profile</Text>
        </TouchableOpacity>
      </View>
    )
  }



const styles = StyleSheet.create({
  container : {
    flex : 1,
    justifyContent : "center",
    alignItems : "center",
    backgroundColor : "#fff",
  }
})

const mapStateToProps = (state) => ({
  profile : state.profile
})

export default connect(mapStateToProps)(ProfileCompleteMessage)
