import React from "react";
import {TextInput, View, Text , StyleSheet, Image, ScrollView, Modal, TouchableHighlight} from "react-native";
import {connect} from "react-redux";
import { Button } from 'react-native-elements';


//component that displays the image for a job if it was posted or a default image
const JobImage = (props) =>{
  let source = require("./assets/noimage.png")
  if(props.image)
  {
    source = {uri : props.image}
  }
  return (
    <View style = {styles.noImageContainer}>
      <Image
        source = {source}
        style = {styles.noimage}/>
    {
      !props.image ? (
        <View style = {styles.noimagetext}>
          <Text style = {styles.noImageMsg}>No Image</Text>
        </View>
      ) :
      null
    }

    </View>
  )
}

const requires = {
  plumber : require(`./assets/plumber.png`),
  carpenter : require(`./assets/carpenter.png`),
  electrician : require(`./assets/electrician.png`),
  painter : require(`./assets/painter.png`)
}



class SearchJobDetail extends React.Component {

  state = {
    modalVisible: false,
    amount : "",
    comment : "",
    errorFlag : false,
    errorMsg : "something went wrong please try again later",
    bidSuccess : false,
    bidsCount : ""
  };

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener("focus", async() => {
      console.log("Search job detail page loaded")
      let response = await fetch(`${this.props.url}getbids`, {
        method:"POST",
        headers : {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
        },
        body : JSON.stringify({
          "id" : this.props.route.params.job.id
        })
      })

      let result = await response.json()
      this.setState({bidsCount : result.bids})
    })
  }

  componentWillUnmount() {
    this._unsubscribe()
  }


  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  }


  handleAmountChange = (value) => {
    this.setState({amount : value})
  }

  handleCommentChange = (value) => {
    this.setState({comment : value})
  }

  //handles the submission of bid from the modal view
  submitBid = async() => {
    try
    {
      //clear any errors if there are any
      this.setState({errorFlag : false})

      let response = await fetch(`${this.props.url}submitbid`, {
        method : "POST",
        headers : {
          "Content-Type" : "application/json",
          Accept : "application/json"
        },
        body : JSON.stringify({
          amount : this.state.amount,
          comment : this.state.comment,
          api_token : this.props.api_token,
          job_id : this.props.route.params.job.id,

        })
      })

      let result = await response.json()
      console.log(result)
      if(result.error)
      {
        this.setState({errorFlag : true, errorMsg : result.errorMsg})
      }

      if(result.success)
      {
        //display bid submission success msg and remove it after 1.5 secs..remove inputs also
        this.setState({
          bidSuccess : true,
          amount : "",
          comment: ""})

        setTimeout(()=> {
          this.setState({bidSuccess : false})
        }, 2000)
      }

    }
    catch (e)
    {
      this.setState({errorFlag : true})
    }

  }


  render (){
    //console.log(this.props)
    const description = this.props.route.params.job.description
    //pushing the images in to ann array if there are any so that render can map a compoent for them
    const images = []
    images.push(this.props.route.params.job.image1, this.props.route.params.job.image2)
    //console.log(images)

    const { modalVisible } = this.state

    return(
      <ScrollView contentContainerStyle = {styles.container}>
        <Text style = {styles.heading}>{this.props.route.params.job.title}</Text>


        <View style = {styles.noImagesContainer}>
        {
          //using a loop to display two JobImage component with two different props value for image
          images.map((image, key) => {
            return(
              <View key = {key} style = {{width : "30%", marginLeft : "3%"}}>
                <JobImage image = {image}/>
              </View>
              )
          })

        }

        </View>

        <View style = {styles.jobTypeSmallIcon}>
          <View style = {{marginRight : "2%"}}>
            <Text style = {styles.jobTypeText}>Job Type:{this.props.route.params.job.type}</Text>
          </View>
          <View style ={styles.jobTypeIconImageContainer}>
            <Image
              source = {requires[this.props.route.params.job.type]}
              style = {styles.jobTypeIconImage}/>
          </View>
        </View>

        <View style = {styles.descriptionContainer}>
          <Text style = {styles.description}>{description}</Text>
        </View>

        {
          this.state.bidsCount ? (
            <View style = {{marginTop : "2%"}}>
              <Text style = {{fontSize : 14}}>Submitted Bids: {this.state.bidsCount}</Text>
            </View>
          ) : null
        }

        <Button
          title = "Bid"
          raised = {true}
          onPress = {()=>{
          this.setModalVisible(true)
          }}
          containerStyle = {{width : "20%", marginTop : "5%"}}/>


        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}

          >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View style = {styles.cancelCross}>
                <TouchableHighlight
                  underlayColor = "antiquewhite"
                  style = {{width : 50, alignItems : "center"}}
                  onPress={() => {
                    this.setModalVisible(!modalVisible);
                }}>
                  <Text style = {{fontSize : 26}}>X</Text>
                </TouchableHighlight>
              </View>
              <Text style={styles.modalText}>Submit Your Bid For This Job</Text>
              <Text style={{...styles.modalText, fontSize : 12}}>Note : If you have already submitted a bid for this Job. A new Bid will replace the previous bid</Text>
              <TextInput
                style = {styles.modalAmountInput}
                placeholder = "Amount"
                onChangeText = {this.handleAmountChange}
                value = {this.state.amount}
                keyboardType = "numeric"/>

              <TextInput
                style = {styles.additionalComments}
                placeholder = "Additional Comments"
                multiline = {true}
                numberOfLines = {4}
                onChangeText = {this.handleCommentChange}
                value = {this.state.comment}/>

              {this.state.errorFlag ? (
                <View style = {{padding : "2%"}}>
                  <Text style = {{color : "red"}}>{this.state.errorMsg}</Text>
                </View>
              ) : null
              }

              {
                //display sucess message for bid submission
                this.state.bidSuccess ? (
                  <View style = {{padding : "2%"}}>
                    <Text style = {{color : "green"}}>Your Bid submitted </Text>
                  </View>
                ): null
              }

              <TouchableHighlight
                style={{ ...styles.openButton, backgroundColor: "#2196F3", marginTop : "2%" }}
                onPress={this.submitBid}
              >
                <Text style={styles.textStyle}>Submit Bid</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>



      </ScrollView>
    )
  }
}


const styles = StyleSheet.create({
  container : {
    backgroundColor : "#fff",
    flex : 1,
    justifyContent : "center",
    alignItems : "center",
  },
  noImageContainer : {
    width : "100%",
    alignItems : "center",
    //backgroundColor : "yellow"
  },
  noimage : {
    width : "100%",
    height : "85%",
    resizeMode : "stretch"

  },
  noimagetext : {
    //backgroundColor : "red",
    position : "absolute",
    top : "76%",
  },
  noImagesContainer : {
    marginTop : "3%",
    width : "100%",
    height : "18%",
    //backgroundColor : "green",
    flexDirection : "row",
    justifyContent : "center"
  },
  heading : {
    fontSize : 24,
    fontWeight : "bold",
    textAlign : "center",
  },
  jobTypeSmallIcon : {
    width : "100%",
    height: 45,
    //backgroundColor : "yellow",
    flexDirection : "row",
    alignItems : "center",
    justifyContent : "flex-end",
    paddingRight : "6%"
  },
  jobTypeIconImageContainer : {
    width: "10%",
  },
  jobTypeIconImage : {
    width : "100%",
    height : "100%",
    resizeMode : "stretch"
  },
  descriptionContainer : {
    marginTop : "5%",
  },
  description : {
    fontSize : 20,
    textAlign : "center",
  },
  jobTypeText : {
    color : "blue",
    fontWeight : "bold"
  },
  noImageMsg : {
    fontSize : 18,
    color : "grey"
  },





  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,

  },
  modalView: {
    width : "70%",
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,

  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  modalAmountInput : {
    width : "60%",
    borderColor : "grey",
    borderWidth : 1,
    padding : 10,
    marginBottom : "3%"
  },
  additionalComments : {
    width : "60%",
    textAlignVertical : "top",
    borderColor : "grey",
    borderWidth : 1,
    padding : 3,
},

  cancelCross : {
    alignSelf : "flex-end"
  }
})


const mapStateToProps = (state) =>({
  url : state.serverUrl,
  api_token : state.api_token,
})


export default connect(mapStateToProps)(SearchJobDetail)
