import React from "react";
import {View, Text, StyleSheet, FlatList, TouchableHighlight, Image} from "react-native";
import {connect} from "react-redux";
import { useNavigation } from '@react-navigation/native';

const InboxList = (props) => {
  console.log("inbox list loaded", props)
  return (
    <FlatList
      data = {props.inboxData}
      renderItem = {(item)=>renderInboxList(item, props)}
      keyExtractor = {(item,index)=> index.toString()}/>
  )
}

const renderInboxList = (obj, props) => {
  //console.log("RENDERINBOX", obj)
  //to display the most recent message in inbox window
  //let lastMesgIndex = obj.item.messages.length - 1
  let messages = obj.item.messages
  //to use it to get the last message in this conversation to display in inbox
  let lastMesgIndex = messages.length - 1
  //console.log("Messages are", test)
  return(
    <TouchableHighlight
      key = {obj.item.key}
      style = {(props.id == obj.item.new_message_for && obj.item.not_seen)?
                (styles.newMessageContainer) : (styles.inboxContainer)}
      underlayColor = "antiquewhite"
      onPress = {()=>props.checkmesg(obj.item.id, obj.item.userInfo.user_id)}>
      <View style= {styles.inboxRow}>
        <View style = {styles.namePicContainer}>
          <Image
            style = {{width : "100%", height : "80%", resizeMode : "stretch"}}
            source = {{uri : obj.item.userInfo.profilePic}}/>
          <Text style= {{fontWeight : "bold"}}>{obj.item.userInfo.name + " " + obj.item.userInfo.last_name}</Text>
        </View>
        <View style = {styles.lastMesgContainer}>
          {
            messages.map((item,index)=>{
              if(index == lastMesgIndex)
              {
                return (<Text
                          key ={index}
                          style = {styles.lastMesg} >{item.message}</Text>)
              }

            })
          }
        </View>
      </View>
    </TouchableHighlight>
  )
}






class Inbox extends React.Component {
  state = {
    errorFlag : false,
    data : null
  }

  componentDidMount()
  {
    this._unsubscribe = this.props.navigation.addListener("focus", async()=> {
      console.log("Sending request in inbox")
      try
      {
        let response = await fetch(`${this.props.url}inbox`, {
          method : "POST",
          headers : {
            "Content-Type": "application/json",
            Accept : "application/json",
          },
          body : JSON.stringify({
            api_token : this.props.api_token
            })
          })


        let result = await response.json()
        console.log(result)
        this.setState({data : result.conversations})
        console.log("State is", this.state.data)
      }
      catch (e)
      {
        this.setState({errorFlag : true})
      }

    })
}


  componentWillUnmount() {
    this._unsubscribe()
  }



  //function to be used when a message thread is clicked in inbox
  checkmesg = async(conversation_id, otherUserId)=> {
    console.log("conversation id:", conversation_id)
    let response =  await fetch(`${this.props.url}messageseen`, {
      method : "POST",
      headers : {
        "Content-Type" : "application/json",
        "Accept" : "application/json",
      },
      body : JSON.stringify({
        conversation_id : conversation_id
      })
    })

    let result = await response.json()
    console.log(result)
    if(result.success)
    {
      //load message window with user id of user with whom the conversation is
      this.props.navigation.navigate("messageWindow", {userIdOfChat : otherUserId})
    }
  }


  render() {
    if(this.state.errorFlag)
    {
      return(
        <View style = {styles.container}>
          <Text>Something went wrong please try again later</Text>
        </View>
      )
    }
    else if(!this.state.data)
    {
      //loading animation
      return (
        <View style ={{...styles.container, alignItems : "center"}}>
          <Image source = {require("./ajax-loader.gif")}/>
        </View>
      )
    }
    else
    {
      return (

        <View style = {styles.container}>
          {
            (this.state.data && this.state.data.length == 0) ?
            (<View style = {{alignItems : "center"}}><Text>No Messages in Your Inbox</Text></View>) :
            (
              <InboxList
                inboxData = {this.state.data}
                id = {this.props.profile.id}
                checkmesg = {this.checkmesg}/>
            )
          }

        </View>
      )
    }

  }
}


const styles = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor : "#fff",
    //justifyContent : "center",
    //alignItems : "center",

  },
  inboxContainer : {
    borderBottomColor: "grey",
    borderBottomWidth : 3,
    width : "100%",
  },

  inboxRow : {
    width : "100%",
    //backgroundColor : "green",
    height : 120,
    flexDirection : "row"
  },
  namePicContainer : {
    width : "35%",
    //backgroundColor : "yellow",
    height : "100%",
    alignItems : "center"
  },
  lastMesgContainer : {
    justifyContent : "center",
    marginLeft : "2%",
    //backgroundColor : "yellow",
    width : "65%",
    //height : "80%"
  },
  lastMesg : {
    padding : 15,
    fontSize : 20,
    flexWrap : "wrap",
    // backgroundColor : "yellow",
    flex : 1,
    //width : "50%"
  },
  newMessageContainer : {
    borderBottomColor: "grey",
    borderBottomWidth : 3,
    width : "100%",
    backgroundColor : "#e3f2fd"
  },
})

const mapStateToProps = (state) => ({
  url : state.serverUrl,
  api_token : state.api_token,
  profile : state.profile

})

export default connect(mapStateToProps)(Inbox)
