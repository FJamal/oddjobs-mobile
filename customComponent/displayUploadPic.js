import React from "react";
import {View, Image, StyleSheet, TouchableOpacity} from "react-native";
import * as ImagePicker from 'expo-image-picker';
import {connect} from "react-redux";
import store from "../store";
import {saveProfile} from "../reducer";




class UploadImage extends React.Component {
  state = {
    image : this.props.profile.profilePic
  }


  selectPic = async() => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      //allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    //console.log(result);
    if(!result.cancelled)
    {
      this.setState({
        image : result.uri
      })

      //adding the new added image's link to temp data in store.
      //this.props.profile.temp.localUri = this.state.image
      let newProfileData = {...this.props.profile}
      newProfileData.temp.localUri = this.state.image
      this.props.addProfile(newProfileData)
      //console.log(store.getState())
    }
  }

  render(){
    return (
      <View style ={styles.profilePicContainer}>
        <View style = {{flex: 1,}}>
          <Image
            source = {
              this.state.image ? ({uri : this.state.image}) : (require("../assets/profile-icon.png"))
            }
            style = {styles.maxHeightWidth}/>
        </View>
        <TouchableOpacity style = {styles.changePicIcon} onPress = {this.selectPic}>
          <Image source = {require("../assets/camera-icon2.png")}
            style = {styles.maxHeightWidth}/>
        </TouchableOpacity>

      </View>
    )
  }
}


const styles = StyleSheet.create({
  profilePicContainer : {
    borderColor : "aqua",
    borderRadius : 5,
    borderWidth : 3,
    width : 175,
    height : 175,

  },
  changePicIcon : {
    //backgroundColor : "blue",
    height : 50,
    width : 50,
    position : "absolute",
    top : "69%",
    left: "69%",
  },
  maxHeightWidth : {
    width : "100%",
    height : "100%"
  }
})


const mapStateToProps = (state) => ({
  profile : state.profile
})

export default connect(mapStateToProps, {addProfile : saveProfile})(UploadImage)
