import {createStore} from "redux";
import {serverUrl, savePostJobType, savePostJobImage} from "./reducer.js";
import Reducer from "./reducer.js";

const store = createStore(Reducer)

console.log(store.getState())

//adding the serverurl in the store
//store.dispatch(serverUrl("http://oddjobs/api/"))
//adding serverurl for TEST ON Device
//store.dispatch(serverUrl("http://192.168.0.102/api/"))

//TEST for postjob
// store.dispatch(savePostJobType("carpenter"))
// store.dispatch(savePostJobImage("1st pic"))
// store.dispatch(savePostJobImage("2nd pic"))

//FOR HEROKU
store.dispatch(serverUrl("https://oddjobs-testing.herokuapp.com/api/"))

//FOR SOCIAL MEDIA POST
//store.dispatch(serverUrl("https://oddjobs-social.herokuapp.com/api/"))

//adding url for testing on tablet
//store.dispatch(serverUrl("http://192.168.0.105/api/"))



console.log(store.getState())
export default store
