import React from "react";
import {FlatList,View, Text, StyleSheet, TextInput, Image} from "react-native";
import {connect} from "react-redux";
import {Button} from "react-native-elements";
import { useHeaderHeight } from '@react-navigation/stack';

//Function that has the view for chat messages of the other user
const OthersChat = (props)=> {
  return(
    <View style = {styles.otherChatContainer}>
      <View style = {styles.otherChatRow}>
        <View style = {styles.userImageContainer}>
          <Image
            source = {{uri : props.profilePic}}
            style = {{width : "100%", height : "100%", resizeMode : "stretch",}}/>
        </View>
        <View style = {styles.otherChatTextContainer}>
          <View style = {{backgroundColor : "#b2dfdb", width : 3, height : 4}}></View>
          <View style = {{backgroundColor : "#b2dfdb", width : 3, height : 5}}></View>
          <View style = {{backgroundColor : "#b2dfdb", width : 3, height : 6}}></View>
          <View style = {{borderRadius : 20, width : "65%",backgroundColor : "#b2dfdb", marginLeft : 0, justifyContent : "center"}}>
            <Text style= {{padding : 8,}}>{props.message}</Text>
          </View>
        </View>

      </View>
    </View>
  )
}

//Function with chat message views for this current user
const YourChat = (props)=> {
  return(
    <View style= {styles.yourChatContainer}>
      <View style = {{marginRight : 8,borderRadius : 20, width : "65%",backgroundColor : "blue", justifyContent : "center"}}>
        <Text style = {{padding : 8 , color : "white"}}>{props.message}</Text>
      </View>
    </View>
  )
}

//Flat list that would render the messages
const MessageThread = (props)=> {
  return(
    <FlatList
      data = {props.messages}
      renderItem = {(item)=>renderMessages(item, props.id, props.profilePic)}
      keyExtractor = {(item, index)=>index.toString()}
      inverted = {1}

      />
  )
}

const renderMessages = (obj, id, profilePic) => {
  if(id == obj.item.user_id)
  {
    return (
      <YourChat message = {obj.item.message}/>
    )
  }
  else
  {
    return (<OthersChat
              message = {obj.item.message}
              profilePic = {profilePic}/>)
  }
}


class MessageWindow extends React.Component {
  state = {
    message : "",
    messages : [],
    profilePic : ""
  }

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener("focus", async()=> {
      let response = await fetch(`${this.props.url}getmessages`, {
        method : "POST",
        headers : {
          "Content-Type" : "application/json",
          Accept : "application/json",
        },
        body: JSON.stringify({
          api_token : this.props.api_token,
          userIdOfChat : this.props.route.params.userIdOfChat,
        })
      })

      let result = await response.json()
      console.log("The conversation gotten from db" , result)
      //save the messages gotten from db into state
      /*bug fix: added the reverse method to have the list scrolled to last
      message using inverted = {1}*/
      if(result.conversation)
      {
        this.setState({
          messages : result.conversation.reverse(),
          profilePic : result.profilePic
        })
      }

    })
  }


  componentWillUnmount() {
    this._unsubscribe();
  }


  handleChange = (message)=> {
    this.setState({message})
  }


  sendMessage = async() => {
    /*remove the message from input field and add into messages*/
    /* althought the object returning from server has more keys but
    message and user_id are the only two required to add a new message*/
    if(this.state.message.length > 0)  //BUG FIX to stop sending empty messages
    {
      let messages = [{
        message : this.state.message,
        user_id : this.props.profile.id
      }, ...this.state.messages,
      ]
      //saving the message but removing from input
      let message = this.state.message
      this.setState({messages ,message : ""})

      let response = await fetch(`${this.props.url}sendmessage`, {
        method : "POST",
        headers : {
          "Content-Type" : "application/json",
          Accept : "application/json",
        },
        body: JSON.stringify({
          api_token : this.props.api_token,
          message : message,
          userIdOfChat : this.props.route.params.userIdOfChat,
        })
      })

      let result = await response.json()
      console.log(result)  
    }

    //scroll to end of list
    // FlatList.scrollToEnd()
    // console.log(FlatList)



  }

  render(){
    //const headerHeight = useHeaderHeight()
    //console.log("Header Height:", headerHeight)
    return (

      <View style= {styles.container}>
        <View style = {{width : "98%", marginTop : "12%",}}>
          <MessageThread
            messages = {this.state.messages}
            id = {this.props.profile.id}
            profilePic = {this.state.profilePic}/>
        </View>

        <View style= {styles.messageComposeContainer}>
          <TextInput
            placeholder = "Type Your Message"
            style = {styles.message}
            onChangeText = {this.handleChange}
            value = {this.state.message}/>

          <Button
            title = "Send"
            buttonStyle = {{marginLeft : "7%"}}
            onPress = {this.sendMessage}/>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor : "#fff",
    alignItems : "center",
    justifyContent : "flex-end",
  },
  messageComposeContainer : {
    width : "100%",
    flexDirection : "row",
    alignItems : "center",
  },
  message : {
    width : "80%",
    borderColor : "grey",
    borderWidth : 2,
    borderRadius : 5,
    padding : 15,
    marginLeft : "2%",
  },
  otherChatContainer : {
    width : "100%",
    //backgroundColor : "yellow",
    //height : "20%",
    marginBottom : "1%",

  },
  userImageContainer : {
    width : "15%",
    height : 80,
    backgroundColor : "teal",
    borderRadius : 40,
    overflow : "hidden"

  },
  otherChatRow : {
    flexDirection : "row",
    alignItems : "center",
  },
  otherChatTextContainer : {
    flexDirection : "row",
    width : "100%",
    alignItems : "center",
    marginLeft : 5,

  },
  yourChatContainer : {
    width : "100%",
    //backgroundColor : "green",
    alignItems : "flex-end",
    marginBottom : "1%"

  }
})


const mapStateToProps = (state) => ({
  url : state.serverUrl,
  api_token : state.api_token,
  profile : state.profile,
})


export default connect(mapStateToProps)(MessageWindow)
