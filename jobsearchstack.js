import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import JobSearch from "./jobsearchpage.js";
import SearchJobDetail from "./searchjobdetail.js";

const jobSearchStack = createStackNavigator();

function searchStack() {
  return (
    <jobSearchStack.Navigator>
      <jobSearchStack.Screen name = "jobSearchMain" component = {JobSearch}
        options = {{
          headerShown : false
        }}/>
      <jobSearchStack.Screen name = "searchJobDetail" component = {SearchJobDetail}
        options = {{title : "Job Details"}}/>
    </jobSearchStack.Navigator>
  )
}


export default searchStack;
