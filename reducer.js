import {combineReducers} from "redux";

export const serverUrl = (url) => ({
  type : "server url",
  payload : url
})


const serverUrlReducer = (state = "", action) => {
  const payload = action.payload
  if (action.type == "server url")
  {
    return payload
  }
  return state
}



export const saveApiToken = (token)=> ({
  type : "api_token",
  payload : token
})

const apitokenReducer = (state = "", action) => {
  const payload = action.payload
  if(action.type == "api_token")
  {
    return payload
  }
  return state
}


export const saveProfile = (data) => ({
  type : "profileData",
  payload : data
})

const profileReducer = (state = [], action) => {
  const payload = action.payload
  if (action.type == "profileData")
  {
    return payload
  }

  return state
}


//to save type of profession needed for a job when user posts a job
export const savePostJobType = (value) => ({
  type : "jobType",
  payload: value
})


// to save the image url in device if user posts pics too for a job
export const savePostJobImage = (value) => ({
  type : "jobImage",
  payload : value
})

export const deletePostJobImage = () =>({
  type: "deletePostJobImage"
})



/*Reducer to take care of profession and images when user submits job.... this
updates the store based on action.type*/
const postJobDataReducer = (state = {type : "", images : []}, action) => {
  const payload = action.payload
  if(action.type == "jobType")
  {
    // state.type = payload
    // return state
    return{
      ...state, ...state.type, type: payload
    }
  }
  if(action.type == "jobImage")
  {
    state.images = [...state.images, payload]
    return state
  }
  // if(action.type == "deletePostJobImage")
  // {
  //   state = undefined
  //   return {type: "", images : []}
  //   return state
  // }
  if(action.type == "deletePostJobImage")
  {
    return {...state,
            // postJobData : {
            //   //...state.postJobData,
            ...state.tpye,
            type : "",
            //...state.images,
            images : []
          }//}
  }

  return state
}



const reducer = combineReducers({
  serverUrl : serverUrlReducer,
  api_token : apitokenReducer,
  profile : profileReducer,
  postJobData : postJobDataReducer,
})

export default reducer;
