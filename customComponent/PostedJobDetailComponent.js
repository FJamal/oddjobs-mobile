import React from "react";
import {View,ScrollView, Text, Image, StyleSheet, Dimensions} from "react-native";
import {Button} from "react-native-elements";
import {MaterialIcons, MaterialCommunityIcons} from '@expo/vector-icons';
import { useRoute } from '@react-navigation/native';


const HEIGHT = Dimensions.get('window').height;
let variableHeight;

//displays an image or a default image for job
const JobImage = (props) =>{
  let source = require("../assets/noimage.png")
  if(props.image)
  {
    source = {uri : props.image}
  }
  return (
    <View style = {styles.noImageContainer}>
      <Image
        source = {source}
        style = {styles.noimage}/>
    {
      !props.image ? (
        <View style = {styles.noimagetext}>
          <Text style = {styles.noImageMsg}>No Image</Text>
        </View>
      ) :
      null
    }

    </View>
  )
}

//for displaying of approriate icons for job type
const requires = {
  plumber : require(`../assets/plumber.png`),
  carpenter : require(`../assets/carpenter.png`),
  electrician : require(`../assets/electrician.png`),
  painter : require(`../assets/painter.png`)
}




const JobCompletedButton = (props) => {

  return (
    <Button
      icon={
        <MaterialIcons name="done" size={26} color="white" />
      }
      title="Job Completed!"
      iconRight = {true}
      containerStyle = {{
        marginTop : 20,
        width : "40%",

      }}
      buttonStyle = {{
        padding : "2%",
        borderRadius : 20
      }}
      titleStyle = {{
        marginRight : "2%"
      }}
      raised = {true}
      onPress = {props.onpress}
      loading = {props.buttonLoading}

    />
  )
}




const UndoJobCompletedButton = (props) => {
  return (
    <Button
      icon={
        <MaterialCommunityIcons name="undo-variant" size={26} color="white" />
      }
      title="Undo Completed!"
      iconRight = {true}
      containerStyle = {{
        marginTop : 20,
        width : "40%",

      }}
      buttonStyle = {{
        padding : "2%",
        borderRadius : 20,
        backgroundColor : "#ef5350",
      }}
      titleStyle = {{
        marginRight : "2%"
      }}
      raised = {true}
      onPress = {props.onpress}

    />
  )
}



const JobDetail = (props) => {
  const route = useRoute();
  const description = route.params.job.description

  //pushing the images in to ann array if there are any so that render can map a compoent for them
  const images = []
  images.push(route.params.job.image1, route.params.job.image2)

  return (
      <ScrollView contentContainerStyle = {styles.topMainContainer}
        style = {{}}>

        <View style = {styles.note}>
          <Text style = {{color : "#1a237e"}}>Note: Please don't forget to mark this job
          as completed when you find a suitable bid and the work is done.</Text>
        </View>
        <Text style = {styles.heading}>{route.params.job.title}</Text>


        <View style = {styles.noImagesContainer}>
        {
          //using a loop to display two JobImage component with two different props value for image
          images.map((image, key) => {
            return(
              <View key = {key} style = {{width : "30%", marginLeft : "3%"}}>
                <JobImage image = {image}/>
              </View>
              )
          })

        }

        </View>

        <View style = {styles.jobTypeSmallIcon}>
          <View style = {{marginRight : "2%"}}>
            <Text style = {styles.jobTypeText}>Job Type:{route.params.job.type}</Text>
          </View>
          <View style ={styles.jobTypeIconImageContainer}>
            <Image
              source = {requires[route.params.job.type]}
              style = {styles.jobTypeIconImage}/>
          </View>
        </View>

        <View
          style = {styles.descriptionContainer}
          onLayout = {(event)=> {
            let {x, y, width, height} = event.nativeEvent.layout;
            console.log("variable height is", height)

          }}>
          <Text style = {styles.description}>{description}</Text>
        </View>

        {
          //display a button only when there is a accepted bid
          (props.acceptedBidFlag && !props.jobCompletedFlag)? (
            <JobCompletedButton
              onpress = {()=>props.handleJobCompletion(props.id, true)}
              buttonLoading = {props.buttonLoading}/>
          ) : null
        }
        {
          //display a undo job completion button if this job is already completed
          (props.jobCompletedFlag) ? (
            <UndoJobCompletedButton
              onpress = {()=>props.handleJobCompletion(props.id, false)}/>
          ) : null
        }


      </ScrollView>

  )
}

// render() {
//   const HEIGHT = Dimensions.get('window').height;
//   return(
//     <View style = {{alignItems : "center", flex: 1}}>
//       <View style = {height : HEIGHT * 40/100,}>
//         <ScrollView>
//           <View>
//             <Text>Fixed Text</Text>
//           </View>
//           <Text>Variable Length Text</Text>
//           //some more Views
//           <View>
//             <Text>Variable Legth Texts</Text>
//           </View>
//           <Button/>
//         </ScrollView>
//         <Text>Bids</Text>
//         //More Stuff
//       </View>
//     </View>
//   )
// }


const styles = StyleSheet.create({
  /*Start of styling of JobDetail*/
  topMainContainer : {
    //height : HEIGHT * 60/100 ,
    height : HEIGHT,
    //height : variableHeight,
    //backgroundColor : "blue",
    //justifyContent : "center",
    alignItems : "center",
    //flexGrow : 1


  },
  note : {
    width : "65%",
    borderWidth : 2,
    borderColor : "#d4e157",
    marginTop : "2%",
    padding : "2%",
    marginBottom : "2%",
  },
  heading : {
    fontSize : 24,
    fontWeight : "bold",
    textAlign : "center",
  },
  noImagesContainer : {
    marginTop : "3%",
    width : "100%",
    height : "28%",
    //backgroundColor : "green",
    flexDirection : "row",
    justifyContent : "center"
  },
  jobTypeSmallIcon : {
    width : "100%",
    height: 45,
    //backgroundColor : "yellow",
    flexDirection : "row",
    alignItems : "center",
    justifyContent : "flex-end",
    paddingRight : "6%"
  },
  jobTypeText : {
    color : "blue",
    fontWeight : "bold"
  },
  jobTypeIconImageContainer : {
    width: "10%",
  },
  jobTypeIconImage : {
    width : "100%",
    height : "100%",
    resizeMode : "stretch"
  },
  descriptionContainer : {
    marginTop : "5%",

  },
  description : {
    fontSize : 20,
    textAlign : "center",
  },

  /*Styling for NoImage Component*/
  noImageContainer : {
    width : "100%",
    alignItems : "center",
    //backgroundColor : "yellow"
  },
  noimage : {
    width : "100%",
    height : "85%",
    resizeMode : "stretch"

  },
  noimagetext : {
    //backgroundColor : "red",
    position : "absolute",
    top : "76%",
  },
  noImageMsg : {
    fontSize : 18,
    color : "grey"
  },
})



export default JobDetail;
