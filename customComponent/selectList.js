/*This component creates a select list using Picker from react-native expects
two props as selectionMesg that is displayed as the first item in the list which
cannot be selected, and the other prop is data, that is used to create the list values.
The selected value is stored in a redux store to be reference by other files*/

import React from "react";
import {View, Text, StyleSheet, Picker} from "react-native";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {savePostJobType} from "../reducer.js";
//import store from "../store.js";
//import { useNavigation } from '@react-navigation/native';

class SelectList extends React.Component {

  static propTypes = {
    selectionMesg : PropTypes.string,
    data : PropTypes.array,
  }

  state = {
    pickerColorFlag : "#C7C7CD",
    val : "",
  }

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      console.log("List Mounted")
      this.setState({val : ""})
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  render(){
    return(

      <Picker
        selectedValue={this.state.val}
        style = {styles.picker}
        onValueChange={(itemValue, itemIndex) => {
          //this.removeErrorMsg()
          if(itemValue !== "0")
          {
            /*POTENTIAL BUG:The this.state.val shows the value of previous selected value in here*/
            this.setState({
              val: itemValue,
              pickerColorFlag : "black"})

              /*SAVE THE SELECTED VALUE IN STORE
              **THIS IS WHAT YOU NEED TO EDIT IF REUSING THIS COMPONENT** */
              this.props.saveJobType(itemValue)


          }
          else
          {
            this.setState({pickerColorFlag : "#C7C7CD"})
          }
        }


        }>

        <Picker.Item label={this.props.selectionMesg} value="0" color = "#C7C7CD" />
        {
          this.props.data.map((value, key) =>(
            <Picker.Item key = {key} label={value} value= {value} />
          ))
        }
      </Picker>


    )
  }
}


const styles = StyleSheet.create({
  container : {
    flex : 1,
    justifyContent : "center",
    alignItems : "center",
    backgroundColor : "blue",

  },
  picker : {
    width: "60%",
    marginTop : "5%",

  },
})

// const test = (state) => ({
//   selection : state.postJobData.type
// })

export default connect(null, {saveJobType : savePostJobType})(SelectList)
